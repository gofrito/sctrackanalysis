#!/usr/bin/env python3
"""
    Created on Mon Jan 27 12:37:39 2014

    processScans.py - starts a scrip to run swspec, CalculateCpp and sctracker
    @author: gofrito
    """

# Import required tools
import os
import subprocess
import sys

try:
    input = raw_input
except NameError:
    pass

def ask_program(prompt):
    program = input(prompt)
    return program

def ask_station(prompt, complaint='Station not supported'):
    while True:
        retries = 3
        station = input(prompt)
        if station in ('Bd','Hh','Ht','Hb','Mc','Mh','Wz','On','Sv','Zc','Ys','Yg','Sh','Ho','Bj','Ww','Ur','Km','Ke','Ef','Wa','Wn','Cd'):
            return station
        else:
            retries = retries -1
        if retries < 0:
            raise IOError('Error in specifying the station')
            print(complaint)

def ask_session(prompt, retries=2, complaint='Session not valid'):
	while True:
		session = input(prompt)
		return session

def ask_format(prompt, retries=2, complaint='Format not supported'):
	while True:
		format = input(prompt)
		if format in  ('MkIV','VLBA','Mk5B','VSIB','VDIF'):
			return format
		else:
			retries = retries -1
		if retries < 0:
			raise IOError('Error to specify data format')
		print(complaint)

def ask_scans(prompt, retries=2, complaint='Only numbers!'):
	while True:
		scans = eval(input(prompt))
		if scans < 500:
			return scans
		else:
			retries = retries - 1
		if retries < 0:
			raise IOError('Error to add scan numbers');
		print(complaint)

def ask_flags(prompt):
    filename = input(prompt)
    fn = open(filename,'r')
    scans = fn.readline()
    flags = scans.split(',')
    return flags

def ask_cores(prompt, retries=2, complaint='Valid number 0 to 6'):
	while True:
		cores = eval(input(prompt))
		if cores < 12:
			return cores
		else:
			retries = retries - 1
		if retries < 0:
			raise IOError('Error to add scan numbers');
		print(complaint)

def filename(prompt, retries=2, complaint='File not found'):
	fn = input(prompt)
	if (os.path.isfile(fn)):
		return fn
	else:
		retries = retries - 1
	if retries < 0:
		raise IOError('The file you entered is not found')
	print(complaint)

def ask_first_scan(prompt, retries=2, complaint='Insert a correct number'):
    while True:
        start = eval(input(prompt))
        if start > 0:
            return start
        else:
            retries = retries -1
        if retries < 0:
            raise IOError('Error with the first scan to process')
        print(complaint)

def run_scans(flag):
    if (program == '0'):
        print('First we create the pertinant folders')
        datapath = '/home/gofrito/data/'
        os.chdir(datapath)

        subprocess.Popen(['mkdir', datapath + session])
        subprocess.Popen(['mkdir', datapath + session + '/' + station])
        fullpath = '{}{}/{}'.format(datapath,session,station)
        os.chdir(fullpath)

        p = subprocess.Popen(['cp', datapath + 'IniFiles/inifile.'+ station,'.'])
        p = subprocess.Popen(['cp', datapath + 'IniFiles/inifile'+ station,'.'])
        p = subprocess.Popen(['cp', datapath + 'IniFiles/OffsetsNone.txt','.'])

        # Replace labels in the inifile for the SWspec
        s = open('inifile.' + station).read()
        s.replace('x000000',session)
        f = open('inifile.' + station,'w')
        f.write(s)
        f.close()

    	# Replace labels in the inifile for the SCtracker
        s = open('inifile' + station).read()
        s = s.replace('x000000', session)
        s = s.replace('x0000', session[0] + session[3:])
        f = open('inifile' + station,'w')
        f.write(s)
        f.close()

        return p

    if (program == '1'):
        scan = fn[12:16]
        scan = '0001'
        s = open('inifile.'+station).read()
        if (i+ncore-1) < 10:
            cn = fn.replace(scan,'000'+str(i+ncore-1))
            s = s.replace('0001','000'+str(i+ncore-1))
        elif (i+ncore-1) < 100:
            cn = fn.replace(scan,'00'+str(i+ncore-1))
            s = s.replace('0001','00'+str(i+ncore-1))
        else:
            cn = fn.replace(scan,'0'+str(i+ncore-1))
            s = s.replace('0001','0'+str(i+ncore-1))
        f = open('inifile.' + station + str(ncore),'w')
        f.write(s)
        f.close()

        print(('Core {}: Running swspectrometer inifile.{}{}{}'.format(str(ncore),station,str(ncore),cn)))         
        p = subprocess.Popen(['swspectrometer','inifile.'+ station + str(ncore),cn])
        return p

    if (program == '2'):
        scan = fn[18:22]
        if i < 10:
           cn = fn.replace(scan,'000'+str(i))
        elif i < 100:
           cn = fn.replace(scan,'00'+str(i))
        else:
           cn = fn.replace(scan,'0'+str(i))
        print(('Running: CalculateCpp.py ' + cn))
        p = subprocess.Popen(['/home/gofrito/pysctrack/bin/calculateCpp.py',cn,'-bw','16','-zf','8.68','8.82','-sc','mex','-rf','8412'])
        return p

    if (program == '3'):
        scan = fn[18:22]
        if i < 10:
           cn = fn.replace(scan,'000'+str(i))
        elif i < 100:
           cn = fn.replace(scan,'00'+str(i))
        else:
           cn = fn.replace(scan,'0'+str(i))
        print(('Running: CalculatePcalCpp.py ' + cn))
        p = subprocess.Popen(['calculatePcalCpp.py',cn])
        return p

    if (program == '4'):
        scan = '0001'
        s = open('inifile'+station).read()
        if (i+ncore-1) < 10:
           s = s.replace(scan,'000'+str(i+ncore-1))
        elif (i+ncore-1) < 100:
           s = s.replace(scan,'00'+str(i+ncore-1))
        else:
           s = s.replace(scan,'0'+str(i+ncore-1))
        f = open('inifile' + station + str(ncore),'w')
        f.write(s)
        f.close()

        print(('Core ' + str(ncore) + ': Running sctraker inifile' + station + str(ncore)))
        p = subprocess.Popen(['sctracker','inifile'+station+str(ncore)])
        return p

    if (program == '5'):
        scan = fn[18:22]
        if i < 10:
           cn = fn.replace(scan,'000'+str(i))
        elif i < 100:
           cn = fn.replace(scan,'00'+str(i))
        else:
           cn = fn.replace(scan,'0'+str(i))
        print(('Running: functionPLL.py ' + cn))
        p = subprocess.Popen(['functionPLL.py',cn])
        return p

    if (program == '6'):
        print('Using SWspec + Polys + SCTracker\n')
        scan = '0001'
        s = open('inifile.'+station).read()
        if (i+ncore-1) < 10:
            cn = fn.replace(scan,'000'+str(i+ncore-1))
            s = s.replace(scan,'000'+str(i+ncore-1))
        elif (i+ncore-1) < 100:
            cn = fn.replace(scan,'00'+str(i+ncore-1))
            s = s.replace(scan,'00'+str(i+ncore-1))
        else:
            cn = fn.replace(scan,'0'+str(i+ncore-1))
            s = s.replace(scan,'0'+str(i+ncore-1))
        f = open('inifile.' + station + str(ncore),'w')
        f.write(s)
        f.close()

        print(('Core ' + str(ncore) + ': Running swspectrometer inifile.' + station + str(ncore) + cn))         
        p = subprocess.Popen(['swspectrometer','inifile.'+ station + str(ncore),cn])
        p.wait()
        print('SWspec has finished correctly \n')

        print('Calculating the polynomials\n')
        
        if (station == 'Wz'):
            form = 'MkIV'
        else:
            form = 'Mk5B'
        
        tmp = os.path.split(cn)
        an = tmp[1]
        dn = an[0] + '14' + an[1:6] + station + '_' + form + '_No0001_1600000pt_1s_ch1_swpsec.bin'
        if i < 10:
           pn = dn.replace(scan,'000'+str(i+ncore-1))
        elif i < 100:
           pn = dn.replace(scan,'00'+str(i+ncore-1))
        else:
           pn = dn.replace(scan,'0'+str(i+ncore-1))
           
        print(('Running: CalculateCpp.py ' + pn))
        p = subprocess.Popen(['calculateCpp.py',pn])
        p.wait()
        
        s = open('inifile'+station).read()
        if (i+ncore-1) < 10:
           s = s.replace(scan,'000'+str(i+ncore-1))
        elif (i+ncore-1) < 100:
           s = s.replace(scan,'00'+str(i+ncore-1))
        else:
           s = s.replace(scan,'0'+str(i+ncore-1))
        
        f = open('inifile' + station + str(ncore),'w')
        f.write(s)
        f.close()

        print(('Core ' + str(ncore) + ': Running sctraker inifile' + station + str(ncore)))
        p = subprocess.Popen(['sctracker','inifile'+station+str(ncore)])
        
        return p

print('Configuring the input parameters\n')

for arg in sys.argv:
    fn = arg

if fn == '-h':
    print('''ProcessScans.py help \n
This python scripts allows to run in parallel several instances of SWspec,\n
Calculate Polynomials and SCtracker\n''')
    print('''To use you need to select between one of each software: \n
Select: 1- SWspec 2-Calculate polynomials 3-Calculate Pcal polys 4- SCtracker \n
1- SWspec requires as input the raw file and a proper Inifile in place\n
2- Calculate Polynomials requires the binary file\n
3- Calculate Polynomials on Phase Cal tone, requires the binary file\n
4- SCtrackers requires the proper Inifile and OffsetFile in place\n
5- Run functionPLL with the tone file\n''')

program = ask_program('Select: 1- SWspec 2-Calculate Polys 3-Calculate Pcal polys 4- SCtracker 5- functionPLL\n')

if (program == '2' or program == '3' or program== '5'):
   #start = ask_first_scan('Which is the first scan? ')
   #scans = ask_scans('Which is the last scan? ')
   flags = ask_flags('Use a file valid scans')
   cores = 1
elif (program == '0'):
   station = ask_station('Which station are you processing? ')
   session = ask_session('Which session are you processing? ')
   cores = 1
   start = 0
   scans = 1
elif (program == '1' or program == '4'):
   station = ask_station('Which station are you processing? ')
   flags = ask_flags('Use a file selecting useful scans for phase-referencing? ')
   cores = ask_cores('How many scans running in parallel? ')
   if len(flags) > 1:
        'We will use the information from the file'
   else:
        start = ask_first_scan('Which is the first scan? ')
        scans = ask_scans('Which is the last scan? ')
   print('\n')

for ip in flags:
   i = int(ip)
   print('ip: {}'.format(ip))
   processes = []
   for ncore in range(1,cores+1):
       print('ncore: {}'.format(ncore))
       print(program)
       p = run_scans(program)
       processes.append(p)
   for p in processes:
       p.wait()
