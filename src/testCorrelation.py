#!/usr/bin/env python3
"""
    Created on Tue Aug 12 14:31:01 2014
    
    testCorrelation.py 
    Generating random signals and the test two methods of correlation
    @author: molera
    """

import numpy as np
import matplotlib.pyplot as plt

def Xcf(d1,d2,Nlag):
    Np = np.size(d1)
    dp2 = np.pad(d2,Nlag/2,'constant')
    lag = np.zeros(Nlag)
    cf = np.zeros(Nlag)
    
    for ip in np.arange(Nlag):
        lag[ip] = ip - Nlag*0.5
        cf[ip] = np.dot(d1,dp2[ip:Np+ip])
    return lag,cf
    
# Moving average from the cluster
def Mav(v,nav):
    nv   = np.size(v)
    nav2 = np.floor(nav/2)
    vp   = np.pad(v,nav-1,'constant')
    va   = np.zeros(nv)
    for jp in np.arange(nv):
        va[jp] = np.divide(1,nav)*np.sum(vp[jp-nav2+nav:jp+nav2+nav])
    return va

Np = 40000
jp = np.arange(Np)
Nmav = 5.
Nlag = 10000

s0 = np.random.random(Np)
s0 = Mav(s0,Nmav)
print(np.std(s0))

sa = np.random.random(Np)
sa = Mav(sa,Nmav)
print(np.std(sa))

sh1 = 0
sh2 = 2
sh3 = 5
sh4 = 25
sh5 = 150
sh6 = 160
sh7 = 180

s1 = s0 + np.roll(sa,sh1)
s2 = s0 + np.roll(sa,sh2)
s3 = s0 + np.roll(sa,sh3)
s4 = s0 + np.roll(sa,sh4)
s5 = s0 + np.roll(sa,sh5)
s6 = s0 + np.roll(sa,sh6)
s7 = s0 + np.roll(sa,sh7)

sc = np.divide(s1+s2+s3+s4+s5+s6+s7,float(7))

lag,xc  = Xcf(s1,s5,Nlag)
lag,xc1 = Xcf(s1-sc,s5-sc,Nlag)


#plt.plot(lag,xc,'b')
plt.plot(lag,xc,'r')
plt.xlim([-200, 200])
plt.show()

print(np.max(xc1))

