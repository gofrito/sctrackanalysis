#!/usr/bin/env python3
"""
    Created on Wed Jun 18 09:01:49 2014

    compileResults.py
    The Frequency detections file contain 5 column with the UTC time since 00:00,
    SNR, Spectral Max, Freq. dets in Hz and Residual Freq. dets. The first 4 lines
    contain the headers with the obesrvation set-up metadata.

    @author: molera
    """
# Import required tools
import numpy as np
import argparse
import os
import AnalysisFunction as af

class cr(object):
    def __init__(self, filename, nfiles,flag):
        print('Initializing the parameters')
        self.filename = filename
        self.nfiles   = nfiles+1
        self.flag     = flag
        self.phase    = np.array([])

    def merge_fdets(self):
        if os.path.isfile(self.filename):
            fd = open(self.filename,'r')
            header = []
            for ip in np.arange(4):
                header.append(fd.readline())
            fd.close()
            first = 1
        for nf in range(1,self.nfiles):
            if nf < 10:
                fdets_file = '{}.000{}{}txt'.format(self.filename[0:-13],str(nf),self.filename[-8:-3])
            elif nf < 100:
                fdets_file = '{}.00{}{}txt'.format(self.filename[0:-13],str(nf),self.filename[-8:-3])
            else:
                fdets_file = '{}.0{}{}txt'.format(self.filename[0:-13],str(nf),self.filename[-8:-3])
            if os.path.isfile(fdets_file):
                if first == 1:
                    self.fdets = af.read_fdets(fdets_file)
                    first = 0
                    print('Opening : {}\033[94m OK \033[0m'.format(fdets_file))
                else:
                    self.fdets = np.concatenate((self.fdets,af.read_fdets(fdets_file)))
                    print('Opening : {}\033[94m OK \033[0m'.format(fdets_file))
            else:
                print('Opening : {}\033[91m Not Found \033[0m'.format(fdets_file))

        FdetsFile = '{}{}txt'.format(self.filename[0:-13],self.filename[-8:-3])
        fdets_header = header[0][2:] + header[1][2:] + header[2][2:]
        np.savetxt(FdetsFile,self.fdets,delimiter=' ',header=fdets_header)
        print('File stored as: {}'.format(FdetsFile))

    def merge_phases(self):
        if os.path.isfile(self.filename):
            fd = open(self.filename,'r')
            header = []
            for ip in np.arange(4):
                header.append(fd.readline())
            fd.close()
            first = 1
        for nf in range(1,self.nfiles):
            if nf < 10:
                phase_file = '{}.000{}.txt'.format(self.filename[0:-9],str(nf))
            elif nf < 100:
                phase_file = '{}.00{}.txt'.format(self.filename[0:-9],str(nf))
            else:
                phase_file = '{}.0{}.txt'.format(self.filename[0:-9],str(nf))

            if os.path.isfile(phase_file):
                if first == 1:
                    self.phase = af.read_phases(phase_file)
                    first = 0
                    print('Opening : {}\033[94m OK \033[0m'.format(phase_file ))
                else:
                    phase = af.read_phases(phase_file)
                    self.phase = np.append(self.phase, np.delete(phase,0,axis=1), axis=1)
                    print('Opening : {}\033[94m OK \033[0m'.format(phase_file ))
            else:
                print('Opening : {}\033[91m Not Found \033[0m'.format(phase_file ))

        PhaseFile = '{}.txt'.format(self.filename[0:-9])
        phase_header = header[0][2:] + header[1][2:] + header[2][2:]
        np.savetxt(PhaseFile, self.phase,delimiter=' ', header=phase_header)
        print('File stored as: {}'.format(PhaseFile))

parser = argparse.ArgumentParser(description='Append several files with Fdets or Phase results')
parser.add_argument('filename', help='First file of Phases or Fdets to merge', nargs='?', default='check_string_for_empty')
parser.add_argument('nfiles', help='Number of files to append', nargs='?', type=int, default=1)
parser.add_argument('--flag', dest='flag', help='flag a scan to be included', nargs='+', type=int, default=0)
args = parser.parse_args()

filename = args.filename
nfiles   = args.nfiles
flag     = args.flag

print('Files to merge: {}'.format(filename))

crobs = cr(filename,nfiles,flag)

if filename[0:5] == 'Fdets':
    crobs.merge_fdets()
elif filename[0:5] == 'Phase':
    crobs.merge_phases()
else:
    print('\033[91mError parsing the input file\033[0m')
