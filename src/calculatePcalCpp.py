#!/usr/bin/env python3
"""
    Created on Fri Jul  4 11:44:23 2014

    CalculateCpp.py - Calculate the first polynomial from the swspec
    @author: gofrito
    """

import numpy as np
import AnalysisFunction as af
import matplotlib.pyplot as plt
import time, os, argparse, re

class calculatePcalCpp:
    def __init__(self):
        self.start = time.perf_counter()
        self.Npf   = 1
        self.Npp   = self.Npf+1
        self.t0    = 0
        self.t1    = 0

        parser = argparse.ArgumentParser()
        parser.add_argument("filename", nargs='?', help="Input spectra file", default="check_string_for_empty")
        parser.add_argument("-p", "--plot", help="Plot some results for Pcal", action="store_true", default=0)
        parser.add_argument('-bw', '--bandwidth', help='Select the recording bandwidth', type=float, default=32)
        parser.add_argument('-f0', '--minfrequency', help='Minimum Frequency', type=float, default=7.98)
        parser.add_argument('-f1', '--maxfrequency', help='Maximum Frequency', type=float, default=8.02)
        parser.add_argument('-rf', '--radiofrequency', help='Sky frequency of the channel', type=float, default=8400)

        args = parser.parse_args()

        self.plot = 0
        if args.plot:
            self.plot = 1

        self.filename = args.filename
        self.BW = args.bandwidth

        path,self.filename = os.path.split(args.filename)
        self.path = '{}/'.format(os.path.abspath(path))
        self.filename = os.path.join(self.path,self.filename)

        match = re.search(r'^([^_]*)_([^_]*)_(?:([^_]*)_)?(?:No|Nc|Nd)?(\d+)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_swspec', self.filename, re.I)
        self.session    = match.group(1)
        self.station    = match.group(2)
        self.rawFormat  = match.group(3)
        self.scanNumber = match.group(4)
        self.FFTpt      = np.int(match.group(5))
        self.dts        = np.float(match.group(6))
        self.channel    = match.group(7)

        if args.bandwidth:
            self.BW    = args.bandwidth
        if args.minfrequency:
            self.Fmin = args.minfrequency
        if args.maxfrequency:
            self.Fmax = args.maxfrequency
        self.Fstart = args.radiofrequency

        self.BW *= 1e6
        self.Fmin *= 1e6
        self.Fmax *= 1e6
        self.Fstart *= 1e6

    def writing_fdets(self):
        if len(self.session) == 7:
            day = '20{}.{}.{}'.format(self.session[1:3],self.session[3:5],self.session[5:7])
        else:
            day = ''

        freq_header = 'Pcal in Base frequency: {:.2} MHz \n'.format(int(self.Fstart)/1e6)

        fdets_fn = '{}Fdets.cal{}.{}.{}.r0i.txt'.format(self.path,day,self.station,self.scanNumber)
        fdets_header = 'Observation conducted on {} at {} rev. 0 \n{}Format :       Modified JD   |       Time(UTC) [s]  | Signal-to-Noise ratio  |       Spectral max     |  Freq. detection [Hz]  |  Doppler noise [Hz] \n'.format(str(day),self.station,freq_header)

        np.savetxt(fdets_fn, np.transpose(self.fdets), newline='\n', header=fdets_header)

    def peak_detection(self):
        # Determine the size of the file and therefore the Number of spectra
        if self.t1 == 0:
            fsize      = os.path.getsize(self.filename)
            self.Nspec = int(np.floor(0.5*fsize/(self.FFTpt + 1)))
            Ts         = self.Nspec*self.dts
        else:
            self.Nspec = self.t1/self.dts

        print('Fmin : {} kHz - Fmax : {} kHz'.format(str(int(self.Fmin/1e3)),str(int(self.Fmax/1e3))))
        print('TimeS : {} s - Num Spectra : {}'.format(str(Ts),str(self.Nspec)))

        SR       = 2*self.BW
        dtsam    = 1/SR
        df       = SR/self.FFTpt
        Hwin     = np.int(1e3/df)
        Avoid    = np.int(100/df)
        Nbins    = np.int(self.FFTpt/2+1);
        jf       = np.arange(Nbins)
        self.ff  = df*jf
        self.tsp = self.dts*(0.5+np.arange(self.Nspec))

        # Read file
        fd = open(self.filename,'rb')

        bfs        = np.int(self.Fmin/df)
        bfm        = np.int(self.Fmax/df)
        Sps        = np.zeros((self.Nspec,bfm-bfs))
        self.Aspec = np.zeros((bfm-bfs))
        ffs        = self.ff[bfs:bfm]
        Tspan      = np.max(self.tsp)

        for ip in np.arange(self.Nspec):
            read_data  = np.fromfile(file=fd, dtype='float32', count=Nbins)
            vspec      = read_data[bfs:bfm]
            Sps[ip]    = vspec*np.hanning(bfm-bfs)
            self.Aspec = Sps[ip] + self.Aspec

        fd.close()

        self.Aspec = np.divide(self.Aspec,self.Nspec)
        mSp        = np.max(np.max(Sps))
        Sps        = np.divide(Sps,mSp)
        self.lSpec = read_data

        xfc  = np.zeros((self.Nspec,3))
        RMS  = np.zeros((self.Nspec,3))
        Smax = np.zeros(self.Nspec)
        SNR  = np.zeros(self.Nspec)
        self.Fdet = np.zeros(self.Nspec)
        dxc  = np.zeros(self.Nspec)
        MJD  = np.zeros(self.Nspec)

        # 'Seeking for the Max and estimating the RMS'
        for ip in np.arange(self.Nspec):
            xfc[ip]  = af.FindMax(Sps[ip],ffs)
            Smax[ip] = xfc[ip,2]
            self.Fdet[ip] = df*xfc[ip,1] + ffs[0]
            RMS[ip]  = af.GetRMS(Sps[ip],xfc[ip,1],Hwin,Avoid)
            SNR[ip]  = (xfc[ip,2] - RMS[ip,0])/RMS[ip,1]

        mSNR = SNR.mean()

        # Calculating the centre of the gravity correction
        for ip in np.arange(self.Nspec):
            dxc[ip] = af.PowCenter(Sps[ip],xfc[ip,1],3)

        dxc    = dxc*df
        FdetC  = self.Fdet + dxc
        Weight = np.power(SNR/mSNR,2)

        Cfs = np.zeros(self.Npp)
        Cps = np.zeros(self.Npp+1)
        Cpr = np.zeros(self.Npp+1)

        Cf   = af.PolyfitW1C(self.tsp,FdetC,Weight,self.Npf)
        Ffit = af.PolyfitW1(self.tsp,FdetC,Weight,self.Npf)
        rFit = FdetC - Ffit

        for ip in np.arange(self.Npp):
            Cfs[ip] = Cf[ip]*np.power(Tspan,-ip)

        for ip in np.arange(1,self.Npp+1):
            Cps[ip] = 2*np.pi*Cfs[ip-1]/ip
            Cpr[ip] = Cps[ip]*np.power(dtsam,ip)

        basefilen = np.size(self.filename) - 12;

        cppname   = '{}.poly{}.txt'.format(self.filename[0:basefilen],str(self.Npp))
        cfsname   = '{}.X{}cfs.txt'.format(self.filename[0:basefilen],str(self.Npf))

        # We force the 2nd coefficient to 0
        Cpr[-1] = Cpr[0]
        Cfs[-1] = Cpr[0]

        np.savetxt(cppname,Cpr)
        np.savetxt(cfsname,Cfs)

        # Reading starting time of the scan
        timebin = '{}_starttiming.txt'.format(self.filename[0:basefilen])
        Tsinfo  = np.loadtxt(timebin,skiprows=1)

        Start        = Tsinfo[1]
        MJD[0:self.Nspec] = Tsinfo[0]

        self.tsp        = self.tsp + Start
        self.fdets = np.array([MJD,self.tsp,SNR,Smax,FdetC,rFit])

        self.writing_fdets()

        print('\n\033[94m- Initial frequency  : {} GHz'.format(str(self.Fstart + FdetC[0]/1e6)))
        print('- Average SNR : {}'.format(str(mSNR)))
        print('- Doppler noise:{}'.format(str(rFit.std())))
        print('- Total elapsed time : {}\033[0m'.format(str(time.perf_counter() - self.start)))

    def print_plots(self):
        if self.plot == 1:
            print('Creating the Figure for plotting the spectra')
            print('Select the figure that you would like to display')
            print('1- Last spectrum read from the file - full bandwdith')
            print('2- Averaged time-integrated spectra - Span zoomed')
            print('3- Frequency detections of the S/C signal')
            print('4- Summary of the Signal detection')
            print('5- Summary of Frequency detection')

            graph = input('Make a choice: 1-2-3-4-5\n')

            if graph == '1':
                plt.plot(self.ff,np.log10(self.lSpec))
                plt.ylabel('Spectrum')
                plt.xlabel('Freq [Hz]')
                plt.title('Last spectrum read from the file')
                plt.show()
            if graph == '2':
                plt.plot(np.log10(self.Aspec))
                plt.ylabel('Spectra')
                plt.xlabel('Freq [Hz]')
                plt.title('Averaged time-integrated spectra')
                plt.show()
            if graph == '3':
                plt.plot(self.Fdet,'ro')
                plt.xlim(0,self.Nspec)
                plt.ylim(self.Fmin,self.Fmax)
                plt.show()
            if graph == '4':
                ax1 = plt.subplot2grid((2,2),(0,0), colspan=2)
                ax1.plot(self.ff,np.log10(self.lSpec))
                ax2 = plt.subplot2grid((2,2),(1,0))
                ax2.plot(np.log10(self.Aspec))
                ax3 = plt.subplot2grid((2,2),(1,1))
                ax3.plot(self.tsp+self.t0,self.Fdet,'ro')
                plt.tight_layout()
                plt.show()
            if graph == '5':
                ax1 = plt.subplot2grid((3,1),(0,0), colspan=1)
                ax1.plot(self.fdets[:,1],self.fdets[:,2],'r')
                ax2 = plt.subplot2grid((3,1),(1,0), colspan=1)
                ax2.plot(self.fdets[:,1],self.fdets[:,4],'bo')
                ax3 = plt.subplot2grid((3,1),(2,0), colspan=1)
                ax3.plot(self.fdets[:,1],self.fdets[:,5],'rx')
                plt.show()

cpc = calculatePcalCpp()
cpc.peak_detection()
cpc.print_plots()
