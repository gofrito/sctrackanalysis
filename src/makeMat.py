#!/usr/bin/env python3
"""
    Created on Mon Jan 27 12:37:39 2014

    makemat.py converts tables to mat, txt or npy format.
    @author: oasis, molera
    """
# Import required tools
import scipy.io as sio
import numpy as np
import sys

table_format = 'new'
base = '/Users/molera/Downloads/ScintObsSummaryTabVEX.Block3.f3.3.102g'

'''
for arg in sys.argv:
    fn = arg

if fn == '-h':
    print 'makemat.py + <summary block observations>'
    print 'Converts the txt file to mat file or viceversa'
else:
    base = fn
    print 'Selected file: ' + base
    print '(Remember if you are using an old format table, please update it in the script)'
'''

#%%
'''
#==============================================================================
# Old table format file
#==============================================================================
'''
if table_format == 'old':
    txt_file = base + '.txt'
    mat_file = base + '.mat'
    npz_file = base + '.npz'
    
    with open(txt_file,'r') as f:
        f_lines = f.readlines()
        
    code = []
    nr = []
    AUdist = []
    Broadening = []
    ErrorSlope = []
    HH = []
    LatEcl = []
    LonEcl = []
    MM = []
    Peak3mHz = []
    PhN = []
    PhSc = []
    SOT = []
    SS = []
    ScintSlope = []
    SolarActA = []
    SolarActB = []
    SysNoise = []
    TEC = []
    az = []
    dd = []
    deh = []
    dem = []
    des = []
    dur = []
    el = []
    mm = []
    ns = []
    nsc = []
    rah = []
    ram = []
    ras = []
    yy = []
    Ceb_Ion_TEC = []
    VEX_Ion_TEC = []
    
    for line in f_lines:
        if line[0]!='/' and line[1]!='_':
            vals = list(map(float, line[1:112].split() + line[114:-3].split()))
            nr.append(vals[0])
            ns.append(vals[1])
            nsc.append(vals[2])
            code.append(vals[3])
            yy.append(vals[4])
            mm.append(vals[5])
            dd.append(vals[6])
            HH.append(vals[7])
            MM.append(vals[8])
            SS.append(vals[9])
            dur.append(vals[10])
            rah.append(vals[11])
            ram.append(vals[12])
            ras.append(vals[13])
            deh.append(vals[14])
            dem.append(vals[15])
            des.append(vals[16])
            az.append(vals[17])
            el.append(vals[18])
            SOT.append(vals[19])
            AUdist.append(vals[20])
            LatEcl.append(vals[21])
            LonEcl.append(vals[22])
            PhN.append(vals[23])
            PhSc.append(vals[24])
            Broadening.append(vals[25])
            ScintSlope.append(vals[26])
            ErrorSlope.append(vals[27])
            Peak3mHz.append(vals[28])
            SysNoise.append(vals[29])
            SolarActA.append(vals[30])
            SolarActB.append(vals[31])
            VEX_Ion_TEC.append(vals[32])
            Ceb_Ion_TEC.append(vals[33])
            TEC.append(vals[34])
    
    
    ''' save to mat file '''
    sio.savemat(mat_file, {'code':np.matrix(code).T, 'nr':np.matrix(nr).T,\
                'AUdist':np.matrix(AUdist).T, 'Broadening':np.matrix(Broadening).T, \
                'ErrorSlope':np.matrix(ErrorSlope).T, 'HH':np.matrix(HH).T, \
                'LatEcl':np.matrix(LatEcl).T, 'LonEcl':np.matrix(LonEcl).T, \
                'MM':np.matrix(MM).T, 'Peak3mHz':np.matrix(Peak3mHz).T, \
                'PhN':np.matrix(PhN).T, 'PhSc':np.matrix(PhSc).T, \
                'SOT':np.matrix(SOT).T, 'SS':np.matrix(SS).T, \
                'ScintSlope':np.matrix(ScintSlope).T, 'SolarActA':np.matrix(SolarActA).T, \
                'SolarActB':np.matrix(SolarActB).T, 'SysNoise':np.matrix(SysNoise).T, \
                'TEC':np.matrix(TEC).T, 'az':np.matrix(az).T, 'el':np.matrix(el).T, \
                'dd':np.matrix(dd).T, 'deh':np.matrix(deh).T, 'dem':np.matrix(dem).T, \
                'des':np.matrix(des).T, 'dur':np.matrix(dur).T, 'mm':np.matrix(mm).T, \
                'ns':np.matrix(ns).T, 'nsc':np.matrix(nsc).T, 'rah':np.matrix(rah).T, \
                'ram':np.matrix(ram).T, 'ras':np.matrix(ras).T, 'yy':np.matrix(yy).T, \
                'Ceb_Ion_TEC':np.matrix(Ceb_Ion_TEC).T, \
                'VEX_Ion_TEC':np.matrix(VEX_Ion_TEC).T})
       
#%%             
else :      
    '''
    #==============================================================================
    # New table format file
    #==============================================================================
    '''    
    txt_file = base + '.txt'
    mat_file = base + '.mat'
    npy_file = base + '.npy'
    
    with open(txt_file,'r') as f:
        f_lines = f.readlines()
    
    code = []
    nr = []
    AUdist = []
    ErrorSlope = []
    HH = []
    LatEcl = []
    LonEcl = []
    MM = []
    Peak3mHz = []
    PhN = []
    PhSc = []
    SOT = []
    STO = []
    SS = []
    ScintSlope = []
    Dnoise = []
    DopSNR = []
    SolarAct = []
    SysNoise = []
    TEC = []
    Tx = []
    az = []
    dd = []
    deh = []
    dem = []
    des = []
    dur = []
    el = []
    mm = []
    ns = []
    nsc = []
    rah = []
    ram = []
    ras = []
    yy = []
    Ceb_Ion_TEC = []
    VEX_Ion_TEC = []
    
    for line in f_lines:
        if len(line)>2 and line[0]!='/' and line[1]!='_':
            #print line[179:-3]
            vals = list(map(float, line[1:117].split() + line[119:175].split() + line[179:-3].split()))
            nr.append(vals[0])
            ns.append(vals[1])
            nsc.append(vals[2])
            code.append(vals[3])
            yy.append(vals[4])
            mm.append(vals[5])
            dd.append(vals[6])
            HH.append(vals[7])
            MM.append(vals[8])
            SS.append(vals[9])
            dur.append(vals[10])
            rah.append(vals[11])
            ram.append(vals[12])
            ras.append(vals[13])
            deh.append(vals[14])
            dem.append(vals[15])
            des.append(vals[16])
            az.append(vals[17])
            el.append(vals[18])
            LatEcl.append(vals[19])
            LonEcl.append(vals[20])
            AUdist.append(vals[21])
            SOT.append(vals[22])
            STO.append(vals[23])
            
            PhSc.append(vals[24])
            PhN.append(vals[25])
            ScintSlope.append(vals[26])
            ErrorSlope.append(vals[27])
            Peak3mHz.append(vals[28])
            SysNoise.append(vals[29])
            Dnoise.append(vals[30])
            DopSNR.append(vals[31])
            
            SolarAct.append(vals[32])
            Tx.append(vals[33])
            VEX_Ion_TEC.append(vals[34])
            Ceb_Ion_TEC.append(vals[35])
            TEC.append(vals[36])
    
    
    ''' save to mat file '''
    sio.savemat(mat_file, {'code':np.matrix(code,dtype=int).T, 'nr':np.matrix(nr,dtype=int).T,\
                'AUdist':np.matrix(AUdist,dtype=float).T, 'Dnoise':np.matrix(Dnoise,dtype=float).T, \
                'DopSNR':np.matrix(DopSNR,dtype=float).T, \
                'ErrorSlope':np.matrix(ErrorSlope,dtype=float).T, 'HH':np.matrix(HH,dtype=int).T, \
                'LatEcl':np.matrix(LatEcl,dtype=float).T, 'LonEcl':np.matrix(LonEcl,dtype=float).T, \
                'MM':np.matrix(MM,dtype=int).T, 'Peak3mHz':np.matrix(Peak3mHz,dtype=float).T, \
                'PhN':np.matrix(PhN,dtype=float).T, 'PhSc':np.matrix(PhSc,dtype=float).T, \
                'SOT':np.matrix(SOT,dtype=float).T, 'SS':np.matrix(SS,dtype=int).T, \
                'ScintSlope':np.matrix(ScintSlope,dtype=float).T, 'SolarAct':np.matrix(SolarAct,dtype=float).T, \
                'STO':np.matrix(STO,dtype=float).T, 'SysNoise':np.matrix(SysNoise,dtype=float).T, \
                'TEC':np.matrix(TEC,dtype=float).T, 'az':np.matrix(az,dtype=float).T, 'el':np.matrix(el,dtype=float).T, \
                'dd':np.matrix(dd,dtype=int).T, 'deh':np.matrix(deh,dtype=int).T, 'dem':np.matrix(dem,dtype=int).T, \
                'des':np.matrix(des,dtype=float).T, 'dur':np.matrix(dur,dtype=int).T, 'mm':np.matrix(mm,dtype=int).T, \
                'ns':np.matrix(ns,dtype=int).T, 'nsc':np.matrix(nsc,dtype=int).T, 'rah':np.matrix(rah,dtype=int).T, \
                'ram':np.matrix(ram,dtype=int).T, 'ras':np.matrix(ras,dtype=float).T, 'yy':np.matrix(yy,dtype=int).T, \
                'Ceb_Ion_TEC':np.matrix(Ceb_Ion_TEC,dtype=float).T, 'VEX_Ion_TEC':np.matrix(VEX_Ion_TEC).T})
          
          
    ''' save to npy file '''
    np.save(npy_file, {'code':np.matrix(code,dtype=int).T, 'nr':np.matrix(nr,dtype=int).T,\
                'AUdist':np.matrix(AUdist,dtype=float).T, 'Dnoise':np.matrix(Dnoise,dtype=float).T, \
                'DopSNR':np.matrix(DopSNR,dtype=float).T, \
                'ErrorSlope':np.matrix(ErrorSlope,dtype=float).T, 'HH':np.matrix(HH,dtype=int).T, \
                'LatEcl':np.matrix(LatEcl,dtype=float).T, 'LonEcl':np.matrix(LonEcl,dtype=float).T, \
                'MM':np.matrix(MM,dtype=int).T, 'Peak3mHz':np.matrix(Peak3mHz,dtype=float).T, \
                'PhN':np.matrix(PhN,dtype=float).T, 'PhSc':np.matrix(PhSc,dtype=float).T, \
                'SOT':np.matrix(SOT,dtype=float).T, 'SS':np.matrix(SS,dtype=int).T, \
                'ScintSlope':np.matrix(ScintSlope,dtype=float).T, 'SolarAct':np.matrix(SolarAct,dtype=float).T, \
                'STO':np.matrix(STO,dtype=float).T, 'SysNoise':np.matrix(SysNoise,dtype=float).T, \
                'TEC':np.matrix(TEC,dtype=float).T, 'az':np.matrix(az,dtype=float).T, 'el':np.matrix(el,dtype=float).T, \
                'dd':np.matrix(dd,dtype=int).T, 'deh':np.matrix(deh,dtype=int).T, 'dem':np.matrix(dem,dtype=int).T, \
                'des':np.matrix(des,dtype=float).T, 'dur':np.matrix(dur,dtype=int).T, 'mm':np.matrix(mm,dtype=int).T, \
                'ns':np.matrix(ns,dtype=int).T, 'nsc':np.matrix(nsc,dtype=int).T, 'rah':np.matrix(rah,dtype=int).T, \
                'ram':np.matrix(ram,dtype=int).T, 'ras':np.matrix(ras,dtype=float).T, 'yy':np.matrix(yy,dtype=int).T, \
                'Ceb_Ion_TEC':np.matrix(Ceb_Ion_TEC,dtype=float).T, 'VEX_Ion_TEC':np.matrix(VEX_Ion_TEC).T})
