#!/usr/bin/env python3
"""
    Created on Sat Jun 14 07:49:56 2014

    CheckSpectra.py plots the spectra of a VLBI observation
    Settings are  placed by default for new VGOS system: 32e6 Hz bandwidth
    @author: molera
    """

import numpy as np
import AnalysisFunction as af
import matplotlib.pyplot as plt
import os, argparse, re

def SelectGraph(prompt):
   graph = input(prompt)
   return graph

class PlotSpectra:
    def __init__(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("-p",  "--plot", help="Plot some results for S/C", action="store_true", default=False)
        parser.add_argument("-o",  "--output", help='Store in a text file the averaged spectra', action='store_true', default=False)
        parser.add_argument("-bw", "--bandwidth", help="Observing bandwidth", type=float, default=32)
        parser.add_argument("-t0", "--initialtime", help="Initial time", type=int, default=0)
        parser.add_argument("-t1", "--endtime", help="Final time", type=int, default=0)
        parser.add_argument("-f0", "--minfrequency", help="Minimum Zoom Frequency", type=float, default=20)
        parser.add_argument("-f1", "--maxfrequency", help="Maximum Zoom Frequency", type=float, default=24)
        parser.add_argument("-rf", "--radiofrequency", help="Starting Radio Frequency", type=float, default=0)
        parser.add_argument("filename", nargs='?', help="Input spectra file", default="check_string_for_empty")
        args = parser.parse_args()

        self.filename    = args.filename
        self.plot        = args.plot
        self.textout     = args.output

        self.t0 = args.initialtime
        self.t1 = args.endtime

        path,self.filename = os.path.split(args.filename)
        self.path = '{}/'.format(os.path.abspath(path))

        match = re.search(r'^([^_]*)_([^_]*)_(?:([^_]*)_)?(?:N)?([oabcdeffh])?(\d+)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_(?:swspec|scspec)', self.filename, re.I)

        self.session    = str(match.group(1))
        self.station    = str(match.group(2))
        self.pol        = str(match.group(4))
        self.scan       = str(match.group(5))
        self.FFTpt      = np.int(match.group(6))
        self.dts        = np.float(match.group(7))

        self.Fsmin  = args.minfrequency*1e6
        self.Fsmax  = args.maxfrequency*1e6
        self.BW     = args.bandwidth*1e6
        self.Fstart = args.radiofrequency

        if self.Fsmin < 0 or self.Fsmax > self.BW:
            print('Zoom Frequency must be within Bandwidth')

        if self.t1 == 0:
            fsize      = os.path.getsize(self.filename)
            self.Nspec = np.int(np.floor(0.5*fsize/(self.FFTpt + 1)))
            self.t1    = self.Nspec*self.dts
        else:
            self.Nspec = np.int(self.t1/self.dts)

        if self.session[0] == 'm':
            self.spacecraft = 'mex'
        else:
            self.spacecraft = 'tbd'

    def processing(self):
        SR      = 2*self.BW
        df      = SR/self.FFTpt
        Nfft    = np.int(self.FFTpt/2+1)
        jf      = np.arange(Nfft)
        self.ff = df*jf

        # Read file
        fd = open(self.filename,'rb')

        bfs    = np.int(self.Fsmin/df)
        bfm    = np.int(self.Fsmax/df)
        Sps    = np.zeros((self.Nspec,bfm-bfs))

        self.Aspec  = np.zeros(bfm-bfs)
        self.ffs    = self.ff[bfs:bfm]

        for ip in np.arange(self.Nspec):
            read_data = np.fromfile(file=fd, dtype='float32', count=Nfft)
            vspec     = read_data[bfs:bfm]
            #vspec     = read_data*np.hanning(Nfft)
            Sps[ip]   = vspec

        fd.close()

        self.Aspec = np.sum(Sps,axis=0)/self.Nspec
        mSp        = Sps.max()
        Sps        = Sps/mSp
        self.lSpec = read_data

        # If text output is enabled dump Sps to a text file
        # Make sure the window is small otherwise the file can be large
        if self.textout == True:
            day       = '20{}.{}.{}'.format(self.session[1:3],self.session[3:5],self.session[5:7])
            ofilename = 'Spec.{}.{}.{}.{}.if{}.txt'.format(self.spacecraft,day,self.station,self.scan,self.pol)

            spec_header = 'Observation conducted on {} at {}\n\n\n'.format(day,self.station)
            spectra = np.transpose([self.ffs,self.Aspec])

            np.savetxt(ofilename, spectra, newline='\n', header=spec_header)

        # Spectra file opened and copied to an array
        xfc       = np.zeros((self.Nspec,3))
        Smax      = np.zeros(self.Nspec)
        self.Fdet = np.zeros(self.Nspec)

        # Seeking for the Max and estimating the RMS
        for ip in np.arange(self.Nspec):
            xfc[ip]       = af.FindMax(Sps[ip],self.ffs)
            Smax[ip]      = xfc[ip,2]
            self.Fdet[ip] = df*xfc[ip,1] + self.ffs[0]

    def generate_plot(self):
       if self.plot == 1:
           print('Creating the Figure for plotting the spectra')
           print('Select the figure that you would like to display')
           print('1- Last spectrum read from the file - full bandwdith')
           print('2- Averaged time-integrated spectra - Span zoomed')
           print('3- Frequency detections of the S/C signal')
           print('4- Summary plot with scan, zoom and Fdet')
           graph = SelectGraph('Make a choice: 1-2-3-4\n')
           epoch = '{}.{}.{}'.format(self.session[5:7],self.session[3:5],self.session[1:3])

           if graph == '1':
               plt.plot(self.Fstart + self.ff,np.log10(self.lSpec))
               plt.ylabel('Spectrum')
               plt.xlabel('Freq [MHz]')
               plt.title('Last spectrum - {} {}'.format(epoch,self.station))
               plt.xlim([0,int(self.BW)])
               plt.show()
           if graph == '2':
               plt.plot((self.Fstart + self.ffs)/1e6,np.log10(self.Aspec))
               plt.ylabel('Spectra')
               plt.xlabel('Freq [Hz]')
               plt.title('Averaged spectra - {} {}'.format(epoch,self.station))
               plt.show()
           if graph == '3':
               plt.plot(self.Fstart + self.Fdet,'ro')
               plt.xlim(0,self.Nspec)
               plt.ylabel('Freq [Hz]')
               plt.xlabel('Time [s]')
               plt.title('Frequency detections of spacecraft')
               plt.show()
           if graph == '4':
               ax1 = plt.subplot2grid((2,2),(0,0), colspan=2)
               ax1.plot(self.Fstart + self.ff,np.log10(self.lSpec))
               ax2 = plt.subplot2grid((2,2),(1,0))
               ax2.plot(self.Fstart + self.ffs,self.Aspec)
               ax3 = plt.subplot2grid((2,2),(1,1))
               ax3.plot(self.Fstart + self.Fdet,'ro')
               plt.tight_layout()
               plt.show()

p = PlotSpectra()
p.processing()
p.generate_plot()
