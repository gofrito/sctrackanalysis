#!/usr/bin/env python3
"""
    Created on Fri Jul  4 11:44:23 2014

    Analysys function to estimate the scintillation coefficients and values
    functionPLL.py - Digital Phase-Locked Loop
    @author: molera
    """

import numpy as np
import AnalysisFunction as af
import matplotlib.mlab as mlab
import matplotlib.pyplot as plt
import scipy.signal as sps
import scipy.stats as stats
import os, argparse, re
import time

class function_PLL:
    def __init__(self):

        # List of parsed parameters
        parser = argparse.ArgumentParser()
        parser.add_argument("filename", nargs='?',  help="Input spectra file")
        parser.add_argument("-p", "--plot",         help="Plot a summary of the results on screen", action="store_true", default=False)
        parser.add_argument("-pp", "--fullplot",    help="Plot all the results on screen", action="store_true", default=False)
        parser.add_argument("-vx", "--vexade",      help="Fast move of the S/C don't use weighted functions based on SNR", action="store_true", default=False)
        parser.add_argument("-pc", "--powercentre", help="Don't use an neighbour average to estimate Frequency Max", action="store_true", default=False)

        parser.add_argument('-p1', '--cpp1', help='First Polynomial', type=int, default=6)
        parser.add_argument('-p2', '--cpp2', help='Second Polynomial', type=int, default=6)
        parser.add_argument('-p3', '--cpp3', help='Third Polynomial', type=int, default=6)

        parser.add_argument('-tadd', '--addtime', help='Time added in SCtracker', type=float, default=0)
        parser.add_argument('-t0', '--starttime', help='Start time', type=float, default=0)
        parser.add_argument('-t1', '--endtime',   help='End time', type=float, default=0)
        parser.add_argument('-nf', '--nfft',      help='Number FFT points', type=int, default=20000)
        parser.add_argument('-ch', '--channel',   help='Number of channel', type=int, default=1)
        parser.add_argument('-dp', '--doppler',   help='Generate Doppler rev. 3', action='store_true', default=False)
        parser.add_argument("-ft", "--forcetime", help="Force integration time if swspec and sctracker use different", type=int, default=5)
        parser.add_argument("-ff", "--forcefft", help="Force number of FFT if swspec and sctracker use different", type=int, default=3200000)

        parser.add_argument("-rf", "--radiofrequency", help="Starting Radio Frequency", type=float, default=0)
        parser.add_argument("-bw", "--bandwidth",      help="Bandwidth", type=float, default=2000.)
        parser.add_argument("-s",  "--spacecraft",     help="Spacecraft")

        parser.add_argument("-bw2", "--bandwidthStep2", help="Bandwidth used with 2nd fit", type=float, default=20)
        parser.add_argument("-bw3", "--bandwidthStep3", help="Bandwidth used with 3rd fit", type=float, default=20)
        parser.add_argument("-bw4", "--bandwidthStep4", help="Bandwidth used for ultra narrow band", type=float, default=0.5)

        parser.add_argument("-ovlp", "--overlap",       help="FFT overlapping factor", type=float, default=2)
        parser.add_argument("-nav",  "--nav",           help="Number of averaged FFT", type=float, default=2)
        parser.add_argument("-padd", "--padding",       help="Padding factor", type=float, default=2)

        args = parser.parse_args()

        self.verbose  = args.plot
        self.fverbose = args.fullplot
        self.vexade   = args.vexade
        self.powcen   = args.powercentre
        self.doppler  = args.doppler
        self.Tadd     = args.addtime
        self.Tskip    = args.starttime
        self.Nfft     = args.nfft
        self.channel  = args.channel
        self.ft       = args.forcetime
        self.fFFT     = args.forcefft
        self.BW       = args.bandwidth
        self.BW2      = args.bandwidthStep2
        self.BW3      = args.bandwidthStep3
        self.BW4      = args.bandwidthStep4
        self.Npp1     = args.cpp1
        self.Npp2     = args.cpp2
        self.Npp3     = args.cpp3
        self.Overlap  = args.overlap
        self.Nav      = args.nav
        self.Padding  = args.padding

        if args.filename:
            tonefile = os.path.split(os.path.abspath(args.filename))
            self.dataDir   = tonefile[0] + '/'
            self.inputfile = tonefile[1]
            self.fil_lng   = len(self.inputfile) - 10

        match = re.search(r'^([^_]*)_([^_]*)_(?:([^_]*))_([^_]*)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_tone', self.inputfile, re.I)

        self.session    = match.group(1)
        self.station    = match.group(2)
        self.rawFormat  = match.group(3)
        self.scanNumber = match.group(4)[2:6]
        self.scanMode   = match.group(4)[0:2]
        self.FFT        = match.group(5)
        self.ints       = match.group(6)
        self.channel    = match.group(7)

        fsize = os.path.getsize(self.inputfile)

        if (args.endtime):
            self.Tspan = args.endtime
        else:
            self.Tspan = np.floor(fsize/(2*8*self.BW))

        self.Tspan = np.floor(self.Tspan)

        # Finding which spacecraft has been observed.
        if args.spacecraft:
            self.spacecraft = args.spacecraft
            self.Fstart = 0.0
        else:
            if self.session[0] == 'v':
                self.Fstart = 8415.99
                self.spacecraft = 'vex'
            elif self.session[0] == 'm':
                self.Fstart = 8415.99
                self.spacecraft = 'mex'
            elif self.session[0] == 'r':
                self.Fstart = 8396.99
                self.spacecraft = 'ros'
            elif self.session[0] == 'g':
                self.Fstart = 8460.99
                self.spacecraft = 'gai'
            elif self.session[0] == 'h':
                self.Fstart = 8468.50
                self.spacecraft = 'her'
            elif self.session[0] == 'k':
                self.Fstart = 8399.50
                self.spacecraft = 'ras'
            else:
                self.spacecraft = 'cal'
                self.Fstart = 0.0

        if args.radiofrequency:
            self.Fstart = args.radiofrequency

    def set_variables(self):
        #==============================================================================
        # Nfft:  Number of FFT points
        # Tadd:  Initial time added to sctracker
        # Tskip: Time to skip into the function PLL
        # Tspan: Ending time
        #==============================================================================
        self.BWi   = self.BW2
        self.BWo   = self.BW3

        self.Npf1  = self.Npp1-1
        self.Npf2  = self.Npp2-1

        log_file = '{}_{}_{}_{}{}_{}pt_{}s_ch{}'.format(self.session,self.station,self.rawFormat,self.scanMode,self.scanNumber,self.FFT,self.ints,self.channel)

        if (self.ints != self.ft):
            if (self.FFT != self.fFFT):
               pol_file = '{}_{}_{}_{}{}_{}pt_{}s_ch{}'.format(self.session,self.station,self.rawFormat,self.scanMode,self.scanNumber,self.fFFT,self.ft,self.channel)
            else:
               pol_file = '{}_{}_{}_{}{}_{}pt_{}s_ch{}'.format(self.session,self.station,self.rawFormat,self.scanMode,self.scanNumber,self.FFT,self.ft,self.channel)
        else:
            if (self.FFT != self.fFFT):
                pol_file = '{}_{}_{}_{}{}_{}pt_{}s_ch{}'.format(self.session,self.station,self.rawFormat,self.scanMode,self.scanNumber,self.fFFT,self.ints,self.channel)
            else:
                pol_file = log_file

        self.tonebin = '{}{}_tonebinning.txt'.format(self.dataDir,log_file)
        self.timebin = '{}{}_starttiming.txt'.format(self.dataDir,log_file)
        self.filecpp = '{}{}.poly{}.txt'.format(self.dataDir,pol_file,str(self.Npp1))
        self.filecfs = '{}{}.X{}cfs.txt'.format(self.dataDir,pol_file,str(self.Npf1))

        if os.path.isfile(self.filecpp)==False:
            print('\033[93mCPP file not found : {}\033[0m'.format(self.cpp))
            raise

    def read_swspec_files(self):
        # Read Tone and Start time to know the beginning of the raw data file
        Tbinfo = np.loadtxt(self.tonebin)
        Tsinfo = np.loadtxt(self.timebin,skiprows=1)

        # We will use the To for the recording start and the initial tone bin
        self.StartF = Tbinfo[3]
        self.StartT = Tsinfo[1]
        self.MJD    = Tsinfo[0]

        # Read the Phase Polynomial Coefficients iteration 1
        self.Cpp1 = np.loadtxt(self.filecpp)
        self.Cfs1 = np.loadtxt(self.filecfs)

    def writing_fdets(self,rev):
        if len(self.session) == 7:
            day = '20{}.{}.{}'.format(self.session[1:3],self.session[3:5],self.session[5:7])
        else:
            day = ''

        self.filefdets = 'Fdets.{}{}.{}.{}.r2i.txt'.format(self.spacecraft,day,self.station,self.scanNumber)

        freq_header = 'Base frequency: {:.2f} MHz \n'.format(self.Fstart/1e6)

        fdets_header = 'Observation conducted on ' + str(day) + ' at ' + self.inputfile[8:10] + ' rev. ' + str(rev) + ' \n' + freq_header +  'Format :       Modified JD   |       Time(UTC) [s]  | Signal-to-Noise ratio  |       Spectral max     |  Freq. detection [Hz]  |  Doppler noise [Hz]\n'

        np.savetxt(self.dataDir + self.filefdets, np.transpose(self.fdets2), newline='\n', header=fdets_header)

    def writing_phase(self):
        if len(self.session) == 7:
            day = '20{}.{}.{}'.format(self.session[1:3],self.session[3:5],self.session[5:7])
        else:
            day = ''

        self.filephase = 'Phases.{}{}.{}.{}.txt'.format(self.spacecraft,day,self.station,self.scanNumber)

        phase_header = 'Residual Phases session ' + day + ' at ' + self.inputfile[8:10] + '\nMJD - Time [s] -  mSNR  - mDoppler noise \n'  + str(int(self.fdets2[0,0])) + ' ' + str(int(self.fdets2[0,1])) + ' ' + str(np.mean(self.fdets2[:,2])) + ' ' + str(np.mean(self.fdets2[:,5])) + '\n  Time stamp [s]    |     Phase [rad]'
        np.savetxt(self.dataDir + self.filephase, self.phase.transpose(), fmt='%.16e', delimiter=' ', newline='\n', header=phase_header)

    def data_processing(self):
        start = time.process_time()
        Sr    = 2*self.BW                  # Sampling Rate
        Nt    = np.int(self.Tspan*Sr)      # Total number of samples
        Ovlp  = self.Overlap
        Nav   = self.Nav
        Padd  = self.Padding

        dt   = 1/Sr                        # Sampling interval
        df   = Sr/self.Nfft
        jt   = np.arange(Nt)
        tw   = dt*self.Nfft
        tt   = jt*dt

        Nspec = np.int(Nt/(self.Nfft*Nav))
        sk    = np.int(self.Tskip*df/2)
        Nspek = Nspec - sk
        jspek = np.linspace(sk,Nspec-1,Nspek)
        Bav   = self.Nfft*Nav
        tspek = (jspek+0.5)*Bav*dt
        Npadd = np.int(self.Nfft*(Padd-1))
        dpadd = np.zeros(Npadd)

        print('Skip spec: ' + str(sk))
        print('Last spec: ' + str(Nspec))

        if Padd == 1:
            Npadd = 1

        jps  = np.arange(self.Nfft)
        Win  = np.power(np.cos(np.pi/self.Nfft*(jps-0.5*self.Nfft+0.5)),2)
        Nfp  = self.Nfft*Padd/2+1
        jfs  = np.arange(Nfp)
        dfs  = 1/(tw*Padd)
        ffs  = jfs*dfs

        # MakeSpec is the core function that reads the tone and outputs the spectra
        Sp  = af.MakeSpec(self.dataDir + self.inputfile,Nspec,self.Nfft,Nav,Ovlp,Win,Padd,dpadd)
        xSp = np.mean(Sp)
        Sp  = np.divide(Sp,xSp)
        Spa = np.divide(Sp.sum(axis=0),Nspec)

        if self.fverbose == 1:
            plt.title('Step 1 - Last integration averaged spectra')
            plt.plot(ffs,np.log10(Spa))
            plt.ylabel('Spectrum')
            plt.xlabel('Frequency [Hz]')
            plt.show()

        HalfWindow    = 40
        LineAvoidance = 10

        xf   = np.zeros((Nspek,3))
        rmsd = np.zeros((Nspek,3))

        dxc    = np.zeros(Nspek)
        SNR    = np.zeros(Nspek)
        Smax   = np.zeros(Nspek)
        Fdet   = np.zeros(Nspek)
        MJD    = np.zeros(Nspek)
        Weight = np.ones((Nspek,1))

        for ip in np.arange(sk,Nspec):
            jp       = ip - sk
            xf[jp]   = af.FindMax(Sp[ip],ffs)
            Smax[jp] = xf[jp,2]
            Fdet[jp] = dfs*xf[jp,1] + ffs[0]
            if self.powcen == 1:
                dxc      = af.PowCenter(Sp[ip],xf[jp,1],3)*dfs
                Fdet[jp] = Fdet[jp] + np.transpose(dxc)
            rmsd[jp] = af.GetRMS(Sp[ip],xf[jp,1],HalfWindow/dfs,LineAvoidance/dfs)
            SNR[jp]  = (xf[jp,2] - rmsd[jp,0])/rmsd[jp,1]

        mSNR  = SNR.mean()
        mFdet = Fdet.mean()

        MJD[0:Nspek] = self.MJD

        if self.vexade == 1:
            Fdet   = Fdet - mFdet
        else:
            Weight = np.power(SNR,2)/mSNR

        Ffit  = af.PolyfitW1(tspek,Fdet,Weight,self.Npf2)
        rFdet = Fdet - Ffit
        Cf2   = af.PolyfitW1C(tspek,Fdet,Weight,self.Npf2)

        if self.verbose == 1:
            plt.suptitle('Step 2 - Frequency Fit')
            plt.subplot(4,1,1)
            plt.plot(ffs,np.log10(Spa))
            plt.ylabel('Spectrum')
            plt.xlabel('Frequency [Hz]')
            plt.subplot(4,1,2)
            plt.plot(tspek,SNR,'rx')
            plt.xlabel('Time [s]')
            plt.ylabel('SNR')
            plt.subplot(4,1,3)
            plt.plot(tspek,Fdet,'r.')
            plt.plot(tspek,Ffit,'b')
            plt.xlabel('Time [s]')
            plt.ylabel('Freq. [Hz]')
            plt.subplot(4,1,4)
            plt.plot(tspek,rFdet,'k.')
            plt.xlabel('Time [s]')
            plt.ylabel('Residual Freq [Hz]')
            plt.show()

        if self.fverbose == 1:
            plt.suptitle('Step 2 - Frequency Fit')
            plt.subplot(3,1,1)
            plt.plot(tspek,SNR,'rx')
            plt.xlabel('Time [s]')
            plt.ylabel('SNR')
            plt.subplot(3,1,2)
            plt.plot(tspek,Fdet,'r.')
            plt.plot(tspek,Ffit,'b')
            plt.xlabel('Time [s]')
            plt.ylabel('Freq. [Hz]')
            plt.subplot(3,1,3)
            plt.plot(tspek,rFdet,'k.')
            plt.xlabel('Time [s]')
            plt.ylabel('Residual Freq [Hz]')
            plt.show()

        if self.fverbose == 1:
            plt.title('Step 2 - Frequency Fit Residuals')
            n, bins, patches = plt.hist(rFdet, density=1)
            (mu, sigma) = stats.norm.fit(rFdet)
            y = stats.norm.pdf(bins, mu, sigma)
            plt.plot(bins, y, 'r--')
            plt.xlabel('Residual Freq [Hz]')
            plt.ylabel('Probability')
            plt.show()
 
        if self.vexade == 1:
            Cf2[0] = Cf2[0] + mFdet

        print('\033[94mStd dev  : ' + str(np.std(rFdet)) + '\033[0m')
        print('\033[94mSNR mean : ' + str(np.mean(SNR)) + '\033[0m')

        Cfs2   = np.zeros(self.Npf2+1)
        Cpp2   = np.zeros(self.Npp2+1)
        Ffirst = np.zeros(Nspek)

        for ip in np.arange(self.Npf2+1):
            Cfs2[ip]   = Cf2[ip]*np.power(self.Tspan-self.Tskip,-ip)
            Cpp2[ip+1] = np.multiply(Cfs2[ip],np.power(ip+1,-1, dtype=float))

        if self.Npp1 < self.Npp2:
            Cfs2[self.Npp1:self.Npp2] = 0

        for ip in np.arange(Nspek):
            for jp in np.arange(1,self.Npp1):
                Ffirst[ip] = Ffirst[ip] + self.Cfs1[jp]*np.power(tspek[ip],jp)

        # Fvideo includes the 1st polynomials the Fdet only the 2nd approach
        Fvideo = self.StartF + Ffirst + Fdet
        if self.vexade == 1:
            Fvideo = Fvideo + mFdet

        tts = tspek + self.StartT + self.Tadd

        # Storing the day of the day, timestamp, SNR, Spectral max, Fdet, Residual Fdets
        self.fdets2 = np.array([MJD,tts,SNR,Smax,Fvideo,rFdet])

        self.writing_fdets(2)

        cppname = self.dataDir + self.inputfile[0:self.fil_lng] + '.poly' + str(self.Npp2) + '.rev2.txt'
        cfsname = self.dataDir + self.inputfile[0:self.fil_lng] + '.X' + str(self.Npf2) + 'cfs.rev2.txt'

        np.savetxt(cppname,Cpp2)
        np.savetxt(cfsname,Cfs2)

        FO      = np.floor(self.BW/self.BWi)
        Nffto   = np.int(self.Nfft/FO)
        Nsegm   = np.int(Nt/self.Nfft)*Ovlp-(Ovlp-1)
        jpso    = np.arange(Nffto)
        Npfo    = Nffto*0.5 + 1
        BWih    = 0.5*self.BWi
        Nto     = np.int(Nt/FO)
        Ntk     = np.int((self.Tspan-self.Tskip)*Sr/FO)
        dto     = dt*FO
        jto     = np.arange(Ntk)
        tto     = jto*dto
        dfto    = 1/(self.Tspan-self.Tskip)
        fto     = dfto*jto
        Oshifti = self.Nfft/Ovlp

        Wini = np.cos(np.pi/self.Nfft*(jps-0.5*self.Nfft+0.5))
        Wino = np.cos(np.pi/Nffto*(jpso-0.5*Nffto+0.5))

        Cf1 = np.zeros(self.Npf2+1)
        Cf2 = np.zeros(self.Npf2+1)

        for ip in np.arange(self.Npp1):
            Cf1[ip] = self.Cpp1[ip+1]*(ip+1)/(2*np.pi)

        for ip in np.arange(self.Npp2):
            Cf2[ip] = Cpp2[ip+1]*(ip+1)/(2*np.pi)

        Cf    = Cf1 + Cf2
        Cf[0] = Cf2[0]
        Cpp   = np.zeros(self.Npp2+1)

        for ip in np.arange(1,self.Npp2+1):
            Cpp[ip] = 2*np.pi*Cf[ip-1]/ip

        Fcc = Cpp2[1]

        ts = np.divide(tt, self.Tspan - self.Tskip)
        res = []
        for jp in np.arange(2,self.Npf2+1):
            res.append(Cpp[jp] * np.power(ts, jp))

        PhDopp = np.multiply(np.sum(np.array(res), axis=0), self.Tspan - self.Tskip)

        Bsc     = np.floor((Fcc-BWih)/df-1) # if we add one here we get exactly one bin less than in Matlab
        Bec     = np.floor(Bsc+Npfo)        # the end bin is exactly the same as python dont count the last
        Fstartc = (Bsc+1)*df                # If we add one here we get exactly the same as matlab
        Pssc    = Fstartc*Oshifti*dt - np.floor(Fstartc*Oshifti*dt)
        Esc     = np.exp(1j*2*np.pi*Pssc)
        Esc     = -Esc

        spf = af.MakeFiltX(self.dataDir + self.inputfile,PhDopp,Bsc,Bec,Esc,Nto,Bav,Nspec,Wini,Wino,Nsegm,self.Nfft,Nffto,Ovlp)

        spf   = spf[sk*Nffto*Nav:]
        ssf   = np.fft.fft(spf)
        ssfp  = np.power(np.abs(ssf),2)
        xssfp = ssfp.max()
        ssfp  = np.divide(ssfp,xssfp)

        if self.fverbose == 1:
            plt.suptitle('Step 2 - Spacecraft tone after phase correction')
            ax = plt.subplot(1,1,1)
            plt.semilogy(fto,ssfp)
            plt.grid(True, which='both', axis="y")
            ax.minorticks_on()
            plt.xlim([0,self.BWi])
            plt.ylim([1e-8,1])
            plt.xlabel('Freq. [Hz]')
            plt.ylabel('Normalized Power')
            plt.show()

        # Determine the frequency of the max power after phase correction
        xft     = af.FindMax(ssfp,fto)
        fmax    = dfto*xft[1]
        spnoise = ssfp[200:900]
        SNR     = np.divide(1,np.std(spnoise))
        dBSNR   = 10*np.log10(SNR)

        #print('Frequency  MAX after PHC: {}'.format(str(fmax)))

        #==============================================================================
        # Select frequencies to rotate the tone
        # targetFreq 0.5 * outputBandwidth
        # rotationFreq = Initial tone Freq - targetFreq
        #==============================================================================
        Ftarg = np.floor(0.05*self.BWo)
        Frot  = fmax - Ftarg

        #==============================================================================
        # sfc:  down converted signal
        # ssf:  down converted FFT
        # ssfp: down converted Power spectrum (normalized)
        #==============================================================================
        sfc   = spf*np.exp(-1j*2*np.pi*Frot*tto)
        ssf   = np.fft.fft(sfc)
        ssfp  = np.power(np.abs(ssf),2)
        ssfp  = ssfp/ssfp.max()

        if self.fverbose == 1:
            plt.title('Step 2 - Power spectrum down converted S/C tone')
            plt.semilogy(fto,ssfp)
            plt.xlim([0,self.BWo/4])
            plt.ylim([1e-9,1])
            plt.ylabel('Power (log scale)')
            plt.xlabel('Frequency [Hz]')
            plt.show()

        #==============================================================================
        # Filtering frequencies above output bandwidth
        # ssf:  down converted filtered FFT
        # ssff: down converted filtered FFT
        # bwo:  output Frequency bin
        #==============================================================================
        ssff       = ssf
        bwo        = np.int(np.where(fto == self.BWo)[0])
        ssff[bwo:] = 0

        #==============================================================================
        # Return to time domain
        # sfc: down converted and filtered signal in time-domain
        #==============================================================================
        sfc = np.fft.ifft(ssff)

        Ampl = np.abs(sfc)
        Ph   = np.angle(sfc)
        Phr  = np.unwrap(Ph)

        if self.fverbose == 1:
            plt.title('Step 2 - Signal Amplitude')
            plt.plot(tto,Ampl,'r')
            plt.ylabel('Amplitude')
            plt.xlabel('Time')
            plt.show()

        #==============================================================================
        # Phase reduction
        # Ph:   phase signal
        # Phr:  unwrapped phase / residual phase
        # dPhr: linear trend free phase (it could be + or -)
        #==============================================================================
        Ph   = np.angle(sfc)
        Phr  = np.unwrap(Ph)
        dPhr = Phr - 2*np.pi*Ftarg*tto


        #==============================================================================
        # Generate the phase polynomial fit (3rd correction)
        # calculate the weighted averaged polynomial fit
        # rdPhr: residual of the linear trend free phase
        #==============================================================================
        wto   = np.ones(Ntk)
        PhFit = af.PolyfitW(tto,dPhr,wto,self.Npp3)
        Cf3   = af.PolyfitWC(tto,dPhr,wto,self.Npp3)

        rdPhr = dPhr - PhFit

        if self.fverbose == 1 or self.verbose == 1:
            plt.suptitle('Step 3 - Summary of the PLL tone detection')
            plt.subplot(2,1,1)
            plt.plot(tto,dPhr,'r')
            plt.plot(tto,PhFit,'b')
            plt.ylabel('phase [rad]')
            plt.subplot(2,1,2)
            plt.plot(tto,rdPhr,'c')
            plt.ylabel('phase [rad]')
            plt.xlabel('time [sec]')
            plt.show()

        #==============================================================================
        # Dopp3: residual of the linear trend free phase
        #==============================================================================
        downRatio     = 10
        numSamples    = len(rdPhr)
        numAvgSamples = np.int(numSamples/downRatio)
        Dopp3         = np.zeros(numAvgSamples)
        ttr           = []

        for ip in range(numAvgSamples):
            ttr.append(tto[ip*downRatio])
            x0      = np.int(ip*downRatio+0.5)
            x1      = np.int(ip*downRatio+1.5)
            Dopp3[ip] = (rdPhr[x0] - rdPhr[x1])/(2*np.pi*downRatio)

        print('\033[94mDoppler rev. 3 rms: ' + str(np.std(Dopp3)) + '\033[0m')

        #==============================================================================
        # Set a boundaries to discard samples at the edges
        # Initial time + 15, End time - 15
        # Store the phase data in Phases.S/C.YYYY.MM.DD.ST.txt format
        #==============================================================================
        tmp   = np.where(tto == 15)
        bmin  = tmp[0][0]
        tmp   = np.where(tto == self.Tspan - self.Tskip - 15)
        bmax  = tmp[0][0]

        print('\033[94mPhase rms: ' + str(np.std(rdPhr[bmin:bmax])) + '\033[0m')
        self.phase = np.array([tto[bmin:bmax],rdPhr[bmin:bmax]])
        p.writing_phase()

        #==============================================================================
        # Correct the downconverted signal with the linear trend free phase
        # sfcc: linear phase signal
        # ssf:  linear phase signal FFT
        #==============================================================================
        sfcc = sfc*np.exp(-1j*PhFit)
        ssf  = np.fft.fft(sfcc)
        ssfp = np.power(np.abs(ssf),2)
        ssfp = ssfp/ssfp.max()

        rmsf = af.GetRMSf(ssfp,fto,Ftarg,0.4*self.BWo,0.1*self.BWo)
        SNR  = np.divide(1-rmsf[0],rmsf[1])

        print('\033[94mDownconverted Freq RMS: ' + str(rmsf) + '\033[0m')
        print('\033[94mDownconverted Freq SNR: ' + str(np.mean(SNR)) + '\033[0m')

        if self.fverbose == 1:
            plt.title('Step 3 - Power spectrum downconverted with the linear trend free phase')
            plt.semilogy(fto,ssfp)
            plt.xlim([0,self.BWo])
            plt.ylim([1e-11,1])
            plt.ylabel('Power (log scale)')
            plt.xlabel('Frequency [Hz]')
            plt.show()

        #==============================================================================
        # Very quick and dirty estimate of the phase scintillation
        # It gives an overlook of the quality of the data
        #==============================================================================
        bss  = np.int(0.003/df + 1.5)
        bse  = np.int(3.003/df + 1.5)
        lph  = np.size(self.phase[1,:])
        wcos = sps.windows.cosine(lph)
        sp   = np.fft.fft(self.phase[1,:])
        spw  = np.fft.fft(np.multiply(self.phase[1,:],wcos))
        pspw = np.power(np.abs(spw),2)
        pspw = 2*pspw/self.BWo
        dta = (self.phase[0,1]-self.phase[0,0])
        Tsa  = dta*lph
        dfa  = 1/Tsa
        ff   = np.arange(0,self.BWi*2-dfa,dfa)

        FiltScint = np.zeros(lph)
        FiltScint[bss:bse] = 2

        spsc   = np.zeros((np.size(self.phase[1,:])),dtype=np.complex64)
        spsc   = np.multiply(sp,FiltScint)
        phs    = np.real(np.fft.ifft(spsc,axis=0))
        rmsphs = np.std(phs, axis=0)

        if self.fverbose == 1:
            print('Phase scintillation RMS: {}'.format(rmsphs))
            plt.loglog(ff,pspw)
            #plt.vlines(0.003,1,1e12,colors='red')
            #plt.vlines(3.003,1,1e12,colors='red')
            plt.xlim([1e-4,10])
            plt.show()
            plt.vlines

        print('Total running time: {}'.format(str(time.process_time() - start)))

        #==============================================================================
        # Ultra phase stop - Filtering to a very narrow bandwidth around the tone
        #==============================================================================
        BWn = self.BW4
        nsfc  = np.fft.fft(sfcc)

        bwna = np.where(fto <= ((np.int(Ftarg) - BWn/2)))
        bwnl = bwna[0][-1]
        bwna = np.where(fto >= ((np.int(Ftarg) + BWn/2)))
        bwnh = bwna[0][0]

        # High pass filter above 0.5 Hz
        nsfc[0:bwnl] = 0
        nsfc[bwnh:]  = 0

        nfc    = np.fft.ifft(nsfc)
        Phn    = np.angle(nfc)
        Phrn   = af.DeWrap(Phn)
        dPhrn  = Phrn - np.pi*(Ftarg*2)*tto
        rdPhrn = dPhrn

        print('\033[94mUltra Phase Stop rms: ' + str(np.std(rdPhrn)) + '\033[0m')

        if self.fverbose == 1:
            plt.title('Step 4 - Phase detection after filtering the signal in ultra narrow band')
            #plt.plot(tto,rdPhrn,'c')
            plt.plot(tto,rdPhr,'r')
            plt.ylabel('phase [rad]')
            plt.xlabel('time [sec]')
            plt.show()

p = function_PLL()
p.set_variables()
p.read_swspec_files()
p.data_processing()

#==============================================================================
#
# Ffirst(1:Nto) = 0;
# for kk=1:Nto
#     for jj=2:Npp1
#         Ffirst(kk) = Ffirst(kk) + Cfs1(jj)*tto(kk)^(jj-1) + Cfs2(jj)*tto(kk)^(jj-1);
#
# dFr(1:Nto) = 0;
#
# for kk=1:Nto-1
#   dFr(kk) = (dPhr3(kk+1) - dPhr3(kk))/dto;
#
# dFr(Nto)=dFr(Nto-1);
#
# Ftarg2 = (BWo - BWn)/2;
# Fdets3 = StartF(1)+ Cfs2(1) + Ffirst + dFr - Ftarg2;
#
# self.fdets3     = zeros((Nto,6))
# self.fdets3(:,1)= tto;               % fdets store spectra time
# self.fdets3(:,2)= Fdets3;            % SNR
# self.fdets3(:,3)= dFr;               % Spectral MAX
#
# p.writing_fdets(3)
#
# day = strcat('20',handles.TonesInput(2:3),'.',handles.TonesInput(4:5),'.',handles.TonesInput(6:7));
# fdets_fn = strcat(handles.TonesPath,'Fdets.',spacecraft,day,'.',handles.TonesInput(9:10),'.',handles.TonesInput(19:22),'.r3i.txt');
#
# nfc   = nfc.*exp(-pi*1i*BWo.*tto)
# nsfc  = fft(nfc)
# nsfc  = abs(nsfc).^2
# xnsfc = max(nsfc)
# nsfc  = nsfc/xnsfc
#==============================================================================
