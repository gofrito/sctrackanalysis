#!/usr/bin/env python3
"""
    Created on Sat Jun 14 07:49:56 2014

    Set of functions used for handling spectra data.
    @author: molera
    """
import numpy as np

#==============================================================================
#    read_phases()      : read phases from the txt file
#    read_fdets()       : read fdets from the txt file
#    mjd()              : calculate the mjd date from a specific date and time
#    hanN()             : calculate the Hanning window of a vector nph times
#    hz2mm()            : transform doppler residual from Hz to mm/s
#    wstdev()           : calculate the Weigthed Standard deviation of a vector
#    FindMax()          : find the peak of a windowed spectral data
#                       : Returns bin, parabolic estimate of max position, max value
#    GetRMS()           : calculate RMS passing a bin window
#    GetRMSf()          : calculate RMS passing a frequency window
#    PowCenter()        : estimate the Power center of the max value of the spectral points
#    PolyfitW()         : calculate the weighted averaged polynomial fit
#    PolyfitW1()        : calculate the weighted polynomial fit
#    ChePolyfit()       : calculate the Chebyshev polynomial fit
#    ChePolyfitW()      : calculate the Chebyshev weighted polynomial fit
#    PolyfitWC()        : calculate the coefficient weighted polynomial fit
#    PolyfitW1C()       : calculate the coefficient weighted averaged polynomial fit
#    MakeSpec()         : calculate the Nfft windowed-overlapped spectra of a signal.
#    MakeWideSpec()     : calculate the NFFT windowed-overlapped spectra on a wide signal
#    MakeFiltX()        : conducts the phase polynomial stop of signal.
#    DeWrap()           : De wraps the 2pi cycles of the residual phase
#==============================================================================

def calc_scans(fdets):
    ts = fdets[1,1] - fdets[0,1]
    ln = np.shape(fdets)
    Nscan = 1
    for ip in np.arange(ln[0]-1):
        if (abs(fdets[ip+1,1] - fdets[ip,1]) > ts):
            Nscan = Nscan + 1

    return Nscan

def read_phases(phase_file):
    phase = np.loadtxt(phase_file,skiprows=4)
    return phase

def read_fdets(fdets_file):
    fdets = np.loadtxt(fdets_file,skiprows=4)
    return fdets

def mjd(yy,mm,dd,HH,MM,SS):
    jdn = 367*yy - (7*(yy+5001+(mm-9)/7))/4+(275*mm)/9+dd+1729777
    jd  = jdn + (HH-12)/24 + MM/1440 + SS/86400
    return jd

def hanN(hspec,nh):
    for ip in np.arange(nh):
        hspec = hspec*np.hanning(np.size(hspec))
    return hspec

def hz2mm(f_nom,doppler):
    stdnoise = doppler*3e8/f_nom
    return stdnoise

def wstdev(x,weights):
    average  = np.average(x, weights=weights)
    variance = np.average((x-average)**2, weights=weights)  # Fast and numerically precise
    return np.sqrt(variance)

def FindMax(Spec, Fscale):
    mx = np.max(Spec)
    px = np.argmax(Spec)

    if px == 0:
        px = 1
    if px == -1:
        px = -2

    a = 0.5*(Spec[px+1] - Spec[px-1])
    b = 0.5*(Spec[px+1] + Spec[px-1] - 2*Spec[px])
    xmax = px - a/(2*b)

    out = (px,xmax,mx)
    return out

def GetRMS(Spec, Fdet, Hwin, Fvoid):
    mw = ww = dw = 0
    fmin   = np.int(Fdet-Hwin)
    fvoid1 = np.int(Fdet-Fvoid)
    fvoid2 = np.int(Fdet+Fvoid)
    fmax   = np.int(Fdet+Hwin)

    mw = np.sum(Spec[fmin:fvoid1]) + np.sum(Spec[fvoid2:fmax])
    ww = 2*(Hwin-Fvoid)
    mm = mw/ww

    vp  = np.power(Spec[fmin:fvoid1]-mm,2)
    vp2 = np.power(Spec[fvoid2:fmax]-mm,2)
    dw = np.sum(vp) + np.sum(vp2)

    rm  = np.sqrt(dw/ww)
    out = (mm,rm,ww)
    return out

def GetRMSf(Spec, Fscale, Fdet, Hwin, Fvoid):
    ns = len(Spec)
    mw = ww = dw = 0

    for ip in np.arange(ns):
        if (np.abs(Fscale[ip]-Fdet) > Fvoid) and (np.abs(Fscale[ip]-Fdet) < Hwin):
            ww = ww + 1
            mw = mw + Spec[ip]

    mm = mw/ww

    for ip in np.arange(ns):
        if (np.abs(Fscale[ip]-Fdet) > Fvoid) and (np.abs(Fscale[ip]-Fdet) < Hwin):
            dw = dw + np.power(Spec[ip]-mm,2)

    rm  = np.sqrt(dw/ww)
    out = (mm,rm,ww)
    return out

def PowCenter(Spec, Xm, Nx):
    xo  = np.int_(Xm+0.5)
    mp  = np.sum(Spec[xo-Nx:xo+Nx+1])
    wp  = np.sum(Spec[xo-Nx:xo+Nx+1]*(np.arange(xo-Nx,xo+Nx+1)-Xm))
    dxo = wp/mp
    return dxo

def PolyfitW(x, y, w, n):
    nx = x.size
    xx = x.max()
    xy = x.min()
    xc = 0.5*(xx+xy)
    xa = 0.5*(xx-xy)
    xn = (x-xc)/xa

    Vp = np.zeros(n+1)
    Mp = np.zeros((n+1,n+1))
    yf = np.zeros(nx)

    for jp in np.arange(n+1):
        Vp[jp] = np.sum(y*w*np.power(xn,jp))
        for ip in np.arange(n+1):
            Mp[jp,ip] = np.sum(np.power(xn,jp+ip)*w)


    Mr = np.linalg.pinv(Mp)
    Cp = np.dot(Mr,Vp)

    for ip in np.arange(nx):
        for jp in np.arange(n+1):
             yf[ip] = yf[ip] + np.power(xn[ip],jp)*Cp[jp]

    return yf

def PolyfitW1(x, y, w, n):
   nx = len(x)
   xx = x.max()
   xn = x/xx

   Vp = np.zeros(n+1)
   Mp = np.zeros((n+1,n+1))
   yf = np.zeros(nx)

   for jp in np.arange(n+1):
        Vp[jp] = np.sum(y*w*np.power(xn,jp))
        for ip in np.arange(n+1):
            Mp[jp,ip] = np.sum(np.power(xn,jp+ip)*w)

   Mr = np.linalg.pinv(Mp)
   Cp = np.dot(Mr,Vp)

   for ip in np.arange(nx):
        for jp in np.arange(n+1):
             yf[ip] = yf[ip] + Cp[jp]*np.power(xn[ip],jp)

   return yf

def ChePolyfit(x, y, n):
   nx = x.size
   xx = x.max()
   xy = x.min()
   xc = 0.5*(xx+xy)
   xa = 0.5*(xx-xy)
   xn = (x-xc)/xa
   
   Vp = np.zeros(n+1)
   Mp = np.zeros((n+1,n+1))
   yf = np.zeros(nx)
      
   for ip in np.arange(n+1):
       Vp[ip] = np.sum(np.Tcheb(ip,xn)*y)
       for jp in range(n+1):
           Mp[ip,jp] = Mp[ip,jp] + np.sum(np.Tcheb(ip,xn)*np.Tcheb(jp,xn))
               
   Mr = np.linalg.pinv(Mp)
   Cp = np.dot(Mr,Vp.T)
   
   for ip in range(nx):
      for jp in range(n): 
         yf[ip] = yf[ip] + np.Tcheb(jp,xn[ip])*Cp[jp]
         
   return yf

def ChePolyfitW(x, y, w, n):
   nx = x.size
   xx = x.max()
   xy = x.min()
   xc = 0.5*(xx+xy)
   xa = 0.5*(xx-xy)
   xn = (x-xc)/xa
   
   Vp = np.zeros(n+1)
   Mp = np.zeros((n+1,n+1))
   yf = np.zeros(nx)

   for ip in np.arange(n+1):
       Vp[ip] = np.sum(np.Tcheb(ip,xn)*y*w)
       for jp in range(n+1):
           Mp[ip,jp] = Mp[ip,jp] + np.sum(np.Tcheb(ip,xn)*np.Tcheb(jp,xn)*w)     
                                      
   Mr = np.linalg.pinv(Mp)
   Cp = np.dot(Mr,Vp.T)
   
   for ip in range(nx):
      for jp in range(n+1): 
         yf[ip] = yf[ip] + np.Tcheb(jp,xn[ip])*Cp[jp]
         
   return yf
   
def PolyfitWC(x, y, w, n):
   xx = np.max(x)
   xy = np.min(x)
   xc = 0.5*(xx+xy)
   xa = 0.5*(xx-xy)
   xn = (x-xc)/xa

   Vp = np.zeros(n+1)
   Mp = np.zeros((n+1,n+1))

   for jp in np.arange(n+1):
        Vp[jp] = np.sum(y*w*np.power(xn,jp))
        for ip in np.arange(n+1):
            Mp[jp,ip] = np.sum(np.power(xn,jp+ip)*w)

   Mr = np.linalg.pinv(Mp)
   Cp = np.dot(Mr,Vp)
   return Cp.T

def PolyfitW1C(x, y, w, n):
   xn = x/np.max(x)

   Vp = np.zeros(n+1)
   Mp = np.zeros((n+1,n+1))

   for jp in np.arange(n+1):
        Vp[jp] = np.sum(y*w*np.power(xn,jp))
        for ip in np.arange(n+1):
            Mp[jp,ip] = np.sum(np.power(xn,jp+ip)*w)
            
   Mr = np.linalg.pinv(Mp)
   Cp = np.dot(Mr,Vp)

   return Cp

def MakeSpec(filename,Nspec,Nfft,Nav,Ovlp,Win,Padd,dpadd):
    Nfp   = np.int(Nfft*Padd/2.+1)
    spa   = np.zeros((Nspec,Nfp))
    Nspav = np.int(Nav*Ovlp-(Ovlp-1))
    Bav   = Nav*Nfft
    fd    = open(filename,'rb')
    dinC = np.fromfile(file=fd, dtype='float32', count=int(2*(Bav*(Nspec+1)+1)))
    tp = dinC[::2] + 1j*dinC[1::2]

    for ip in np.arange(Nspec):
        for jp in np.arange(Nspav):
            abin = np.int(Bav*ip+jp*Nfft/Ovlp)
            bbin = np.int(abin+Nfft)
            din  = tp[abin:bbin]
            din  = np.multiply(din,Win)
            if Padd > 1:
                dinp = np.append(din,dpadd)
            else:
                dinp = din
            sp = np.fft.fft(dinp)

            spa[ip,:] = spa[ip,:] + np.power(np.real(sp[0:Nfp]),2) + np.power(np.imag(sp[0:Nfp]),2)
    fd.close()

    return spa

def MakeWideSpec(filename,Nspec,Nfft,Nav,Ovlp,Win,Padd,dpadd):
    Nfp   = np.int(Nfft*Padd/2.+1)
    spa   = np.zeros((Nspec,Nfp))
    Nspav = np.int(Nav*Ovlp-(Ovlp-1))
    Bav   = Nav*Nfft
    fd    = open(filename,'rb')
    #tp  = np.fromfile(file=fd, dtype='float64', count=-1)
    dinC  = np.fromfile(file=fd, dtype='float16', count=-1)
    tp    = dinC[::2] + 1j*dinC[1::2]

    for ip in np.arange(Nspec):
        for jp in np.arange(Nspav):
            abin = np.int(Bav*ip+jp*Nfft/Ovlp)
            bbin = np.int(abin+Nfft)
            din  = tp[abin:bbin]
            din  = np.multiply(din,Win)
            if Padd > 1:
                dinp = np.append(din,dpadd)
            else:
                dinp = din
            sp = np.fft.fft(dinp)

            spa[ip,:] = spa[ip,:] + np.power(np.real(sp[0:Nfp]),2) + np.power(np.imag(sp[0:Nfp]),2)
    fd.close()

    return spa

def MakeFiltX(filename,Phcorr,Fbinstart,Fbinend,Es,Nto,Bav,Nspec,Wini,Wino,Nsegm,Npsi,Npso,Ovlp):
    fout    = np.zeros(Nto,dtype=complex)
    Npfo    = np.int(Npso/2)
    Oshifti = np.int(Npsi/Ovlp)
    Oshifto = np.int(Npso/Ovlp)
    dpadd   = np.zeros((Npfo-1,1))
    fd      = open(filename,'rb')
    dinC    = np.fromfile(file=fd, dtype='float32', count=int(2*(Bav*(Nspec+1)+1)))
    tp      = dinC[::2] + 1j*dinC[1::2]

    for ip in np.arange(Nsegm):
        skip    = ip*Oshifti
        din     = tp[skip:skip+Npsi]
        din     = (din-127)*Wini
        phc     = Phcorr[skip:skip+Npsi]
        ephc    = np.exp(1j*phc)
        din     = din*ephc
        sp      = np.fft.fft(din)
        spo     = sp[int(Fbinstart):int(Fbinend)]
        spo[0]  = np.real(spo[0])
        spo[-1] = np.real(spo[-1])
        spop    = np.append(spo,dpadd)
        out     = np.fft.ifft(spop)
        dout    = out*Wino
        for jp in np.arange(Npso):
            fout[jp+ip*Oshifto] = fout[jp+ip*Oshifto] + np.multiply(dout[jp],np.power(Es,ip))
    fd.close()
    return fout

def MakeWideFiltX(filename,Phcorr,Fbinstart,Fbinend,Es,Nto,Bav,Nspec,Wini,Wino,Nsegm,Npsi,Npso,Ovlp):
    fout    = np.zeros(Nto,dtype=complex)
    Npfo    = np.int(Npso/2.)
    Oshifti = np.int(Npsi/Ovlp)
    Oshifto = np.int(Npso/Ovlp)
    dpadd   = np.zeros((Npfo-1,1))
    fd      = open(filename,'rb')
    #tp      = np.fromfile(file=fs, dtype='float64', count=-1)
    tp      = np.fromfile(file=fd, dtype='complex16', count= -1)

    for ip in np.arange(np.int(Nsegm)):
        skip    = ip*Oshifti
        din     = tp[skip:skip+Npsi]
        din     = (din-127)*Wini
        phc     = Phcorr[skip:skip+Npsi]
        ephc    = np.exp(1j*phc)
        din     = din*ephc
        sp      = np.fft.fft(din)
        spo     = sp[int(Fbinstart):int(Fbinend)]
        spo[0]  = np.real(spo[0])
        spo[-1] = np.real(spo[-1])
        spop    = np.append(spo,dpadd)
        out     = np.fft.ifft(spop)
        dout    = out*Wino
        for jp in np.arange(Npso):
            fout[jp+ip*Oshifto] = fout[jp+ip*Oshifto] + np.multiply(dout[jp],np.power(Es,ip))
    fd.close()
    return fout

def DeWrap(ph,nargouts=1):
    nph = len(ph)
    dph = np.zeros(nph)
    qph = np.zeros(nph)
    phc = np.zeros(nph)

    for ip in np.arange(1,nph):
        tmp = ph[ip] - ph[ip-1]
        if (np.abs(tmp) < np.pi):
            dph[ip] = 0
        else:
            dph[ip] = np.sign(ph[ip]-ph[ip-1])
    for ip in np.arange(1,nph):
        qph[ip] = qph[ip-1] + dph[ip]

    phc = ph - 2*np.pi*qph
    return phc

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
