#!/usr/bin/env python3
"""
    Created on Fri Jul 25 18:41:11 2014
    checkFdets.py - Graphic and statistics mode
    I- plot the several columns listed in the original final
    II- output on the screen the statistics of the file

    @author: gofrito
    """

import matplotlib.pyplot as plt
import AnalysisFunction as af
import numpy as np
import argparse

parser = argparse.ArgumentParser()

parser.add_argument("filename", nargs='?', help="Input file with Doppler detections")
parser.add_argument("-p", "--plot",        help="Plot standard results on screen", action="store_true", default=False)
parser.add_argument("-s", "--statistics",  help="Show a number of statistics on screen", action="store_true", default=False)

args       = parser.parse_args()

filename   = args.filename
plot       = args.plot
statistics = args.statistics

fdets      = af.read_fdets(filename)

if plot == True:
    plt.figure()
    plt.title('SNR, Frequency detection [Hz] and residual noise [mHz]')
    ax1 = plt.subplot2grid((3,1),(0,0))
    ax1.plot(fdets[:,1],fdets[:,2],'b')
    plt.ylabel('SNR')
    ax2 = plt.subplot2grid((3,1),(1,0))
    ax2.plot(fdets[:,1],fdets[:,4],'ro')
    plt.ylabel('Frequency [Hz]')
    ax3 = plt.subplot2grid((3,1),(2,0))
    ax3.plot(fdets[:,1],fdets[:,5],'bx')
    plt.ylabel('Freq noise [mHz]')
    plt.xlabel('Time serie')
    plt.tight_layout()
    plt.show()

if statistics == True:
    Nscans = int(af.calc_scans(fdets))
    ttotal = fdets[-1,1] - fdets[0,1]
    mSNR = np.mean(fdets[:,2])
    Fmax = np.max(fdets[:,4])
    Fmin = np.min(fdets[:,4])
    dFreq = Fmax - Fmin
    rDopp = np.std(fdets[:,5])
    flags = np.zeros(Nscans)
    sDop  = np.zeros(Nscans)
    sSNR  = np.zeros(Nscans)

    if Nscans > 1 :
        shap = np.shape(fdets)
        ts = fdets[1,1] - fdets[0,1]
        ltotal = shap[0]
        jp = 0
        lscans = np.zeros(Nscans+1, dtype=int)
        for ip in range(ltotal-1):
            if (abs(fdets[ip+1,1] - fdets[ip,1]) > 2*ts):
                 lscans[jp+1] = ip+1
                 jp = jp + 1
        lscans[-1] = -1
        for ip in range(Nscans):
            sDop[ip] = np.std(fdets[lscans[ip]:lscans[ip+1],5])
            sSNR[ip] = np.mean(fdets[lscans[ip]:lscans[ip+1],2])
            if sDop[ip] > 1:
                flags[ip] = 1

    print('Number of scans {} and samples {}'.format(Nscans,np.shape(fdets)[0]))
    print('\033[94mAverage SNR: {:.1f}'.format(mSNR))
    print('Central Frequency detection: {:.1f}'.format(Fmax-dFreq/Nscans))
    print('Doppler velocity: {:.2f} Hz/s'.format(np.divide(dFreq,ttotal)))
    print('                  {:.2f} Hz/scan'.format(np.divide(dFreq,Nscans)))
    print('Doppler residual: {:.2f} mHz\033[0m\n'.format(rDopp*1e3))
    #print('Standard Dev:     {:.2f} mHz\033[0m\n'.format(np.std(rDop)*1e3))

    if Nscans > 1:
      for ip in range(Nscans):
          if flags[ip] == 1:
              print('\033[91mScan no {} std noise {:.2f} mHz & SNR of {:.1f} dB\033[0m'.format(ip+1,sDop[ip]*1e3,10*np.log10(sSNR[ip])))
          else:
              print('\033[92mScan no {} std noise {:.2f} mHz & SNR of {:.1f} dB\033[0m'.format(ip+1,sDop[ip]*1e3,10*np.log10(sSNR[ip])))
