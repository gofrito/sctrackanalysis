#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May 13 14:49:11 2019

@author: gmolera
"""
import numpy as np
import matplotlib.pyplot as plt
import AnalysisFunction as af

class function_PCAL:
    def __init__(self):
        self.filename = 'f190510_Hb_VDIF_No0001_160000pt_1s_ch1_pcal.bin'
        self.datadir  = '/Users/gmolera/tmp/'
        self.Nfft = 2000
        print(self.filename)

    def set_variables(self):
        print('set up variables')

    def processing(self):
        fd = open(self.datadir + self.filename,'rb')
        Sp = np.fromfile(file=fd, dtype='float32', count=-1)
        plt.figure()
        plt.plot(Sp)
        plt.show()

        #Sp  = af.MakeSpec(self.dataDir + self.filename,Nspec,self.Nfft,Nav,Ovlp,Win,Padd,dpadd)
        print('read the binary file')

p = function_PCAL()
p.processing()
