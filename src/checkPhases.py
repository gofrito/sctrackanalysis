#!/usr/bin/env python3
"""
    Created on Mon Jun 15 2020
    checkPhases.py - Graphic and statistics mode
    I- plots the phase fluctuation with time
    II- output on the screen the statistics of the file

    @author: pradyumna
    """

import matplotlib.pyplot as plt
import numpy as np
import argparse
import scipy.signal as sps

def read_phases(phases_file):
    phases = np.loadtxt(phases_file)#,skiprows=4)
    return phases



parser = argparse.ArgumentParser()

parser.add_argument("filename", nargs='?', help="Input file with Doppler detections")
parser.add_argument("-p", "--plot",        help="Plot standard results on screen", action="store_true", default=False)
parser.add_argument("-s", "--statistics",  help="Show a number of statistics on screen", action="store_true", default=False)

args       = parser.parse_args()

filename   = args.filename
plot       = args.plot
statistics = args.statistics

phases      = read_phases(filename)

fss = 0.003
fse = 3.003
fns = 4.
fne = 7.
flo = 0.003
fhi = 0.25

if plot == True:
    plt.figure()
    plt.title('Phase fluctuations')
    plt.plot(phases[:,0],phases[:,1],'b')
    plt.ylabel('Phase')
    plt.xlabel('Time series')
    plt.tight_layout()
    plt.show()
            

if statistics == True:
        phshape = np.shape(phases)
        nph = phshape[1] - 1
        lph = phshape[0]

        ph = np.zeros((lph,nph))
        tt = np.zeros((lph,1))

        tt = phases[:,0]
        ph = phases[:,1:]
        dtp = (tt[1]-tt[0])
        Ts  = dtp*lph
        df  = 1/Ts
        BW  = lph * df
        ff  = np.arange(0,BW,df)
        sp  = np.fft.fft(ph,axis=0)
        psp = np.power(np.abs(sp),2)
        psp = 2*psp/BW

        wcos = sps.cosine(lph)
        tmp  = np.zeros((lph,nph))
        for ip in np.arange(nph):
            tmp[:,ip] = np.multiply(ph[:,ip],wcos)

        spw  = np.fft.fft(tmp,axis=0)
        pspw = np.power(np.abs(spw),2)
        pspw = 2*pspw/BW

        # Plot the power spectra in units of rad2 per Hz
        pspa  = np.multiply(1./nph,np.sum(psp, axis=1))
        pspaw = np.multiply(1./nph,np.sum(pspw, axis=1))
        bss  = int(np.floor(fss/df + 1.5))
        bse  = int(np.floor(fse/df + 1.5))
        bns  = int(np.floor(fns/df + 1.5))
        bne  = int(np.floor(fne/df + 1.5))
        bbeg = int(np.floor(flo/df + 0.5))
        bend = int(np.floor(fhi/df + 0.5))
        snm  = np.mean(pspaw[bns:bne])
        PeakScint = pspaw[bbeg]*1e-4
        ffs  = ff[bbeg:bend]
        pss  = pspaw[bbeg:bend]
        
        # Lffs = np.log10(ffs)
        # Lpss = np.log10(pss)
        FiltScint = np.zeros(lph)
        FiltNoise = np.zeros(lph)
        FiltScint[bss:bse] = 2
        FiltNoise[bns:bne] = 2
        spsc = np.zeros((lph,nph),dtype=np.complex64)
        spno = np.zeros((lph,nph),dtype=np.complex64)
        for ip in np.arange(nph):
            spsc[:,ip] = np.multiply(sp[:,ip],FiltScint)
            spno[:,ip] = np.multiply(sp[:,ip],FiltNoise)

        phs = np.real(np.fft.ifft(spsc,axis=0))
        phn = np.real(np.fft.ifft(spno,axis=0))

        rmsphs = np.std(phs,axis=0)
        rmsphn = np.std(phn,axis=0)

        rmsphsc = np.sqrt(np.power(rmsphs,2) - np.power(rmsphn,2))

        print('Phase scint  : {}'.format(rmsphsc))
        print('Scint  mean  : {:.3f}'.format(np.mean(rmsphsc)))
        print('Standard dev : {:.3f}'.format(np.std(rmsphsc)))

        