#!/usr/bin/env python3
"""
    Created on Fri Jul  4 11:44:23 2014

    Analysys function to estimate the scintillation coefficients and values
    ScintAnalysis.py - manually input expr and station
    @author: molera
    """

import matplotlib.pyplot as plt
import AnalysisFunction as af
import numpy as np
import scipy.signal as sps
import telnetlib, time, os

expr     = 'm200401'
station  = 'Cd'
#dataDir  = '/Users/gmolera/Sync/SpaceP/ScintData/Data/{}/{}/'.format(expr,station)
dataDir  = '/Volumes/SpaceCraft/MarsExpress/{}/{}/'.format(expr,station)
startt   = '04:50'    # Initial time + 10' (to centre it)
stopt    = '06:40'    # Any end time
base     = 00         # Last scan computed
expt     = 0          # Last session computed
pcal     = 'False'    # Calculate instrumental noise of the receiver
gstation = 2          # Indicate the ground station code by default 2

#==============================================================================
#    read_fdets()          : read fdets from the txt file
#    calc_scans()          : calculate the number of scans in the Fdets file
#    station_code()        : returns the the code of a given station
#    station_coord()       : returns the Long, Lat and Height of a given station code
#    return_ephemerides()  : retrieve this session' ephemerides from Horizon web site
#    extract_ephemerides() : extract the relevant fields from the ephemerides buffer
#    convert_time()        : convert time from 00:00 into hours and seconds
#==============================================================================
def calc_scans(fdets):
    ts = fdets[1,1] - fdets[0,1]
    ln = np.shape(fdets)
    Nscan = 1
    for ip in np.arange(1,ln[0]):
        if fdets[ip,1] - fdets[ip-1,1] > ts:
            Nscan = Nscan + 1
    if fdets[0,0] < fdets[-1,0]:
        Nscan = Nscan + 1
    return Nscan

def station_code(antenna_code):
    Tantennas = 27
    antenna = ['Mh','Mc','Ma','Nt',
               'Wz','Ys','Pu','On',
               'Hh','Sh','Km','Ur',
               'Ht','Sv','Zc','Bd',
               'T6','Ww','Ho','Hb',
               'Yg','Ke','Wa','Wn',
               'Wd','Ul','Cd']
    code = np.arange(1,Tantennas+1)
    codeInd = code[antenna.index(antenna_code)]
    return codeInd

def station_coord(antenna_code):
    coor = ['24.3930,60.2178,0.07\n','11.6469,44.5205,0.067\n','16.7040,40.6495,0.543\n','14.9890,36.8760,0.143\n',
            '12.8774,49.1449,0.67\n','356.9131,40.5247,0.988\n','37.6283,54.8206,0.239\n','54.393056,11.917778,0\n',
            '27.6844,-25.8898,1.37\n','121.19944,31.0992,0.029\n','102.7959,25.0273,1.974\n','87.1781,43.4715,2.033\n',
            '27.6844,-25.8898,1.37\n','29.78194,60.5322,0.086\n','41.565,43.787,1.175\n','102.2338,51.7703,0.821\n',
            '121.1360,31.0921,0.049\n','174.6631,-36.4347,0.127\n','147.439167,-42.805,0.043\n','147.439167,-42.805,0.043\n',
            '115.3467,-29.0464,0.244\n','132.1527,-14.3754,0.203\n','174.6631,-36.4347,0.127\n','12.8774,49.1449,0.67\n',
            '12.8774,49.1449,0.67\n','126.9430,37.5622,0.5\n','133.8098,-31.8677,0.164\n']
    coord = coor[antenna_code-1]
    return coord

def return_ephemerides(self):
    HOST = 'horizons.jpl.nasa.gov'
    PORT = 6775
    quantities = '1, 4, 18, 20, 23, 24 \n'

    tn = telnetlib.Telnet(HOST,PORT)
    time.sleep(5)

    if self.sc == 'm':
        tn.write('499\n'.encode('ascii'))
    elif self.sc == 'v':
        tn.write('299\n'.encode('ascii'))
    elif self.sc == 'r':
        tn.write('16543\n'.encode('ascii'))
    time.sleep(5)
    tn.read_until('<cr>:'.encode('ascii'))

    tn.write('E\n'.encode('ascii'))
    tn.read_until(' : '.encode('ascii'))
    tn.write('o\n'.encode('ascii'))
    tn.read_until(' : '.encode('ascii'))
    tn.write('coord\n'.encode('ascii'))
    tn.read_until(' : '.encode('ascii'))
    tn.write('g\n'.encode('ascii'))
    tn.read_until(' : '.encode('ascii'))
    tn.write(station_coord(self.code).encode('ascii'))
    tn.read_until(' : '.encode('ascii'))
    start = '{}-{}-{} {}\n'.format(self.yy,self.mm,self.dd,self.startt)
    if int(self.stopt[0:2]) < int(self.startt[0:2]):
        self.dd = self.dd.replace(self.dd[0:2],str(int(self.dd[0:2])+1))
    stop  = '{}-{}-{} {}\n'.format(self.yy,self.mm,self.dd,self.stopt)   
    tn.write(start.encode('ascii'))
    tn.read_until(' : '.encode('ascii'))
    tn.write(stop.encode('ascii'))
    tn.read_until(' : '.encode('ascii'))
    tn.write('20m\n'.encode('ascii'))
    tn.read_until(' : '.encode('ascii'))
    tn.write('y\n'.encode('ascii'))
    tn.read_until(' : '.encode('ascii'))
    tn.write(quantities.encode('ascii'))
    ephemerides = tn.read_until(' ? : '.encode('ascii'))

    return ephemerides

def extract_ephemerides(eph,nph):
    start = eph.index('SOE'.encode('ascii')) + 6
    total = 134
    yy = []
    mm = []
    dd = []
    HH = []
    MM = []
    rah = []
    ram = []
    ras = []
    dech = []
    decm = []
    decs = []
    az = []
    el = []
    eclLon = []
    eclLat = []
    delta = []
    sot = []
    sto = []
    for ip in np.arange(nph):
        yy.append(eph[start + ip*total      : start + ip*total + 4])
        mm.append(eph[start + ip*total + 5  : start + ip*total + 8])
        dd.append(eph[start + ip*total + 9  : start + ip*total + 11])
        HH.append(eph[start + ip*total + 12 : start + ip*total + 14])
        MM.append(eph[start + ip*total + 15 : start + ip*total + 17])
        rah.append(eph[start + ip*total + 22 : start + ip*total + 24])
        ram.append(eph[start + ip*total + 25 : start + ip*total + 27])
        ras.append(eph[start + ip*total + 28 : start + ip*total + 32])
        dech.append(eph[start + ip*total + 34 : start + ip*total + 37])
        decm.append(eph[start + ip*total + 38 : start + ip*total + 40])
        decs.append(eph[start + ip*total + 41 : start + ip*total + 45])
        az.append(eph[start + ip*total + 45 : start + ip*total + 51])
        el.append(eph[start + ip*total + 55 : start + ip*total + 60])
        eclLon.append(eph[start + ip*total + 64 : start + ip*total + 69])
        eclLat.append(eph[start + ip*total + 74 : start + ip*total + 78])
        delta.append(eph[start + ip*total + 82 : start + ip*total + 88])
        sot.append(eph[start + ip*total + 111: start + ip*total + 117])
        sto.append(eph[start + ip*total + 124: start + ip*total + 129])

    return(yy,mm,dd,HH,MM,rah,ram,ras,dech,decm,decs,az,el,eclLon,eclLat,delta,sot,sto)

def convert_time(seconds):
    hours   = np.floor(seconds/3600)
    minutes = np.floor((seconds-3600*hours)/60)

    return [int(hours),int(minutes)]

class scintAnal(object):
    def __init__(self, dataDir, startt, stopt, base, expt, expr, station, gstation):
        if (expr[0]=='v'):
            spacecraft = 'vex'
        elif (expr[0]=='m'):
            spacecraft = 'mex'
        elif (expr[0]=='r'):
            spacecraft = 'ros'

        self.yy = '20' + expr[1:3]
        self.mm = expr[3:5]
        self.dd = expr[5:7]
        self.sc = expr[0]
        self.code = station_code(station)

        phaseFn = 'Phases.{}{}.{}.{}.{}.txt'.format(spacecraft,self.yy,self.mm,self.dd,station)
        fdetsFn = 'Fdets.{}{}.{}.{}.{}.r2i.txt'.format(spacecraft,self.yy,self.mm,self.dd,station)

        self.phase_file = dataDir + phaseFn
        self.fdets_file = dataDir + fdetsFn

        self.summ_file  = '{}{}sum.{}.{}.txt'.format(dataDir,phaseFn[0:7],phaseFn[7:20],phaseFn[-6:-4])

        self.fss = 0.003
        self.fse = 3.003
        self.fns = 4.
        self.fne = 7.
        self.flo = 0.003
        self.fhi = 0.3

        self.verbose = 1

        # We account for different summary table depending if it's VEX or MEX experiment
        # By default I manually input Mars status values.
        self.base = self.expt = 0
        if self.sc == 'm':
            self.base = base
            self.expt = expt
        elif self.sc == 'v':
            self.base = 1196
            self.expt = 161
        elif self.sc == 'r':
            self.base = 0
            self.expt = 0

        self.startt = startt
        self.stopt  = stopt
        self.gstation = gstation

    def process_scintillation(self):
        phase = af.read_phases(self.phase_file)
        phshape = np.shape(phase)
        nph = phshape[1] - 1
        lph = phshape[0]

        ph = np.zeros((lph,nph))
        tt = np.zeros((lph,1))

        tt = phase[:,0]
        ph = phase[:,1:]

        if self.verbose == 1:
            plt.plot(tt,ph[:,0],'r')
            plt.show()

        # Calculate standard stadistics for each phase scan
        dtp = (tt[1]-tt[0])
        Ts  = dtp*lph
        df  = 1/Ts
        BW  = lph * df
        ff  = np.arange(0,BW,df)

        print('Max phase : {:.3f} \nMin phase : {:.3f}'.format(np.max(ph),np.min(ph)))
        print('Avg phase : {:.3f} \nStd phase : {:.3f}'.format(np.mean(ph),np.std(ph)))

        '''
            # Calculate the power spectra in 2 different ways:
            # 1 Std power spectra and normalize by the bandwidth
            # 2 Calculate the spectra using a cosine window
            '''
        sp  = np.fft.fft(ph,axis=0)
        psp = np.power(np.abs(sp),2)
        psp = 2*psp/BW

        wcos = sps.cosine(lph)
        tmp  = np.zeros((lph,nph))
        for ip in np.arange(nph):
            tmp[:,ip] = np.multiply(ph[:,ip],wcos)

        spw  = np.fft.fft(tmp,axis=0)
        pspw = np.power(np.abs(spw),2)
        pspw = 2*pspw/BW

        # Plot the power spectra in units of rad2 per Hz
        pspa  = np.multiply(1./nph,np.sum(psp, axis=1))
        pspaw = np.multiply(1./nph,np.sum(pspw, axis=1))

        '''
            # Compute the system noise level
            # System noise mean (snm) estimated as the mean value in noise band
            # Peaks of the scintillation is at 3 mHz (PeakScint)
            '''
        bss  = int(np.floor(self.fss/df + 1.5))
        bse  = int(np.floor(self.fse/df + 1.5))
        bns  = int(np.floor(self.fns/df + 1.5))
        bne  = int(np.floor(self.fne/df + 1.5))
        bbeg = int(np.floor(self.flo/df + 0.5))
        bend = int(np.floor(self.fhi/df + 0.5))

        snm  = np.mean(pspaw[bns:bne])
        PeakScint = pspaw[bbeg]*1e-4

        print('Sys noise : {:.3f}'.format(snm*1e3))
        print('Peak Scin : {:.3f}'.format(PeakScint))

        ffs  = ff[bbeg:bend]
        pss  = pspaw[bbeg:bend]
        Lffs = np.log10(ffs)
        Lpss = np.log10(pss)

        Lf     = np.polyfit(Lffs,Lpss,1)
        Lfit   = Lf[1] + Lf[0]*Lffs
        dLfit  = Lpss - Lfit
        rdLfit = np.std(dLfit)

        if self.verbose == 1:
            plt.loglog(ff,pspa,'r')
            plt.xlim(0.001,10)
            plt.ylim(1e-2,1e10)
            plt.xticks([0.001,0.01,0.1,1,10],['0.001','0.01','0.1','1','10'])
            plt.show()

        Slope  = Lf[0]
        errSlope = np.divide(rdLfit,(np.max(Lffs)-np.min(Lffs)))

        print('Slope     : {:.3f}'.format(Slope))
        print('Err Slope : {:.3f}\n'.format(errSlope))

        '''
            # Estimate the phase scint in the scintillation band
            # and from the noise band
            '''
        FiltScint = np.zeros(lph)
        FiltNoise = np.zeros(lph)
        FiltScint[bss:bse] = 2
        FiltNoise[bns:bne] = 2
        spsc = np.zeros((lph,nph),dtype=np.complex64)
        spno = np.zeros((lph,nph),dtype=np.complex64)

        for ip in np.arange(nph):
            spsc[:,ip] = np.multiply(sp[:,ip],FiltScint)
            spno[:,ip] = np.multiply(sp[:,ip],FiltNoise)

        phs = np.real(np.fft.ifft(spsc,axis=0))
        phn = np.real(np.fft.ifft(spno,axis=0))

        rmsphs = np.std(phs,axis=0)
        rmsphn = np.std(phn,axis=0)

        rmsphsc = np.sqrt(np.power(rmsphs,2) - np.power(rmsphn,2))

        print('Phase scint  : {}'.format(rmsphsc))
        print('Scint  mean  : {:.3f}'.format(np.mean(rmsphsc)))
        print('Standard dev : {:.3f}'.format(np.std(rmsphsc)))

        # We read from the Fdets file r2i and extract the Doppler noise of the scans        
        fdets = af.read_fdets(self.fdets_file)
        Nscans = calc_scans(fdets)
        timeStamp = np.zeros((Nscans,2))
        rDop = np.zeros(Nscans)
        SNR  = np.zeros(Nscans)
        start_time = convert_time(fdets[0,1])
        print('\nNumber of scans: {}\nInitial time:    {}:{}'.format(Nscans,start_time[0],start_time[1]))
        shap = np.shape(fdets)
        ltotal = shap[0]

        if Nscans > 1 :
            lscan = int(ltotal/Nscans)
            for ip in range(Nscans):
                timeStamp[ip,:] = convert_time(fdets[lscan*ip,1])
                rDop[ip] = 1000*np.std(fdets[lscan*ip:(ip+1)*lscan,5]) # maybe add plus one somewheretestSyst
                SNR[ip] = np.mean(fdets[lscan*ip:(ip+1)*lscan,2])
        else :
            lscan = ltotal/2
            timeStamp[0] = convert_time(fdets[0,1])
            rDop[0] = 1000*np.std(fdets[0:lscan,5])
            SNR[0]  = np.mean(fdets[0:lscan,2])

        '''
        for ip in np.arange(nph):
            if (os.path.exists(dataDir + phaseFn[0:20] + '.' + phaseFn[-6:-4] + '000' + str(nph) +'.txt'))
                # The phase exists so we read the start time from the Fdets file
                fdet = read_fdets(self.fdets_file[0:-5] + '000' + str(nph) + '.txt')
                seconds = fdet[0,1]
                start_time = convert_time(seconds)
                '''

        if pcal == 'False':
            data_out = np.zeros((nph,37))
            eph = return_ephemerides(self)
            eyy,emm,edd,ehour,emin,erah,eram,eras,edech,edecm,edecs,eaz,eel,eeclLon,eeclLat,edelta,esot,esto = extract_ephemerides(eph,nph)

            for ip in np.arange(nph):
                data_out[ip] = [self.base + int(ip+1), self.expt+1, int(ip+1), self.code, self.yy ,self.mm, self.dd, ehour[ip], int(emin[ip])-10, 00, 1140, erah[ip], eram[ip], eras[ip], edech[ip], edecm[ip], edecs[ip], eaz[ip], eel[ip], eeclLon[ip], eeclLat[ip], edelta[ip], esot[ip], esto[ip], rmsphs[ip], rmsphn[ip], Slope, errSlope, PeakScint, snm, rDop[ip], SNR[ip], 0, self.gstation, 30.0, 30.0, 1.0]
                np.savetxt(self.summ_file, data_out, fmt='| %4d %3d %3d %3d %7d %2d %2d %3d %2d %2d %5d %3d %2d %4.1f %3d %2d %4.1f %5.1f %2.1f %5.1f %6.1f %7.4f %6.2f %6.2f |%8.3f %6.3f %7.3f %2.3f %8.1f %7.2f %7.2f %7.0f |%3d %6d %5.1f %7.1f %8.1f |', delimiter='  ', newline='\n', header=' Obs  Run Scan St         Date/Time       Dur   Target Coordinates   Local Az/El     Ecl      Dist    SOT     STO |  Scint  SysN    Scint.spec   PeakSPD  SysN   Dnoise   Carr | Solar     Ionosphere TEC    IPS  |')

        else:
            data_out = np.zeros((nph,16))
            for ip in np.arange(nph):
                self.summ_file = self.summ_file.replace('vex','cal')
                data_out[ip] = [int(ip+1), self.expt+1, int(ip+1), self.code, self.yy ,self.mm, self.dd, 00, rmsphs[ip], rmsphn[ip], Slope, errSlope, PeakScint, snm, rDop[ip], SNR[ip]]
                np.savetxt(self.summ_file, data_out, fmt='| %4d %3d %3d %3d %7d %d %d %i %.3f %.3f %.3f %.3f %.2f %.2f %.2f %.0f ', delimiter='  ', newline='\n', header=' Obs  Run Scan St         Date/Time       Dur   Target Coordinates   Local Az/El   Ecl       Dist    SOT     STO |  Scint  SysN    Scint.spec   PeakSPD  SysN   Dnoise   Carr | Solar     Ionosphere TEC    IPS  |')

if (os.path.isdir(dataDir)):
    print(('The data directory is {}'.format(dataDir)))
else:
    dataDir = dataDir[:-3]
    print(('The data directory is {}'.format(dataDir)))

scint = scintAnal(dataDir, startt, stopt, base, expt, expr, station, gstation)
scint.process_scintillation()
