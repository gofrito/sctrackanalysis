#!/usr/bin/env python3
"""
    Created on Fri Jul  4 11:44:23 2014

    Analysys function to estimate the scintillation coefficients and values
    functionPLL.py - Digital Phase-Locked Loop
    @author: molera
    """

import numpy as np
import AnalysisFunction as af
import matplotlib.pyplot as plt
import scipy.signal as sps
import os, time
import argparse

expr    = 'm0403'
station = 'Ht'
scan    = '0001'
dataDir = '~/data/' + expr[0] + '15' + expr[1:5] + '/'
filename = expr[0] +'15' + expr[1:] + '_' + station + '_Mk5B_No' + scan + '_1600000pt_5s_ch1_tone0.bin'

class function_PLL:
    def __init__(self, dataDir, filename):
        self.fil_lng   = len(filename) - 10
        self.inputfile = filename
        self.dataDir   = dataDir

        # List of parsed parameters
        parser = argparse.ArgumentParser()
        parser.add_argument("filename", nargs='?', help="Input spectra file")
        parser.add_argument("-p", "--plot", help="Plot a summary of the results on screen", action="store_true", default=False)
        parser.add_argument("-pp", "--fullplot", help="Plot all the results on screen", action="store_true", default=False)
        parser.add_argument("-vx", "--vexade", help="Fast move of the S/C don't use weighted functions based on SNR", action="store_true", default=False)
        parser.add_argument("-pc", "--powercentre", help="Don't use an neighbour average to estimate Frequency Max", action="store_true", default=False)

        parser.add_argument('-p1', '--cpp1', help='First Polynomial', type=int, default=6)
        parser.add_argument('-p2', '--cpp2', help='Second Polynomial', type=int, default=6)
        parser.add_argument('-p3', '--cpp3', help='Third Polynomial', type=int, default=6)

        parser.add_argument('-tadd', '--addtime', help='Time added in SCtracker', type=float, default=0.)
        parser.add_argument('-t0', '--starttime', help='Start time', type=float, default=0.)
        parser.add_argument('-t1', '--endtime', help='End time', type=float,default=1120.)
        parser.add_argument('-nf', '--nfft', help='Number FFT points', type=float, default=20000.)
        parser.add_argument('-ch', '--channel', help='Number of channel', type=int, default=1)
        parser.add_argument('-dp', '--doppler', help='Generate Doppler rev. 3', action='store_true', default=True)

        args = parser.parse_args()

        self.verbose  = args.plot
        self.fverbose = args.fullplot
        self.vexade   = args.vexade
        self.powcen   = args.powercentre
        self.doppler  = args.doppler
        self.Tadd     = args.addtime
        self.Tskip    = args.starttime
        self.Nfft     = args.nfft
        self.channel  = args.channel

        self.Npp1     = args.cpp1
        self.Npp2     = args.cpp2
        self.Npp3     = args.cpp3

        if args.filename:
            self.inputfile = args.filename
            tonefile = os.path.split(os.path.abspath(args.filename))
            self.dataDir = tonefile[0] + '/'

        fsize = os.path.getsize(self.inputfile)

        if (args.endtime):
            self.Tspan = args.endtime
        else:
            self.Tspan = np.floor(fsize/(4000.*8))

        self.Tspan = np.floor(self.Tspan/10.)*10

    def set_variables(self):
        #==============================================================================
        # Nfft:  Number of FFT points
        # Tadd:  Initial time added to sctracker
        # Tskip: Time to skip into the function PLL
        # Tspan: Ending time
        #==============================================================================
        self.BW    = 200.
        self.BWo   = 20.
        self.BWi   = 20.

        self.Npf1  = self.Npp1-1
        self.Npf2  = self.Npp2-1


    def writing_fdets(self,rev):
        day = ('20' + self.inputfile[1:3] + '.' + self.inputfile[3:5] + '.' + self.inputfile[5:7])
        self.filefdets = ('Fdets.'+ self.spacecraft + day + '.' + self.inputfile[8:10] + '.' + self.inputfile[18:22] + '.r2i.txt')

        fdets_header = 'Observation conducted on ' + str(day) + ' at ' + self.inputfile[8:10] + ' rev. ' + str(rev) + ' \n' + freq_header +  'Format : Time(UTC) [s]  | Signal-to-Noise ratio  |       Spectral max     |  Freq. detection [Hz]  |  Doppler noise [Hz] \n'
        np.savetxt(self.dataDir + self.filefdets, np.transpose(self.fdets2), newline='\n', header=fdets_header)

    def writing_phase(self):
        day = ('20' + self.inputfile[1:3] + '.' + self.inputfile[3:5] + '.' + self.inputfile[5:7])
        self.filephase = 'Phases.' + self.spacecraft + day + '.' + self.inputfile[8:10] + '.' + self.inputfile[18:22] + '.txt'

        phase_header = 'Residual Phases session ' + day + ' at ' + self.inputfile[8:10] + '\nMJD - Time [s] -  mSNR  - mDoppler noise \n'  + str(int(self.fdets2[0,0])) + str(int(self.fdets2[0,1])) + str(np.mean(self.fdets2[:,2])) + str(np.mean(self.fdets2[:,5])) + '\n  Time stamp [s]    |     Phase [rad]'
        np.savetxt(self.dataDir + self.filephase, self.phase.transpose(), fmt='%.16e', delimiter=' ', newline='\n', header=phase_header)

    def data_processing(self):
        Sr    = 2*self.BW
        Nt    = self.Tspan*Sr
        Ovlp  = 2.
        Nav   = 2.
        Padd  = 4.

        dt   = 1./Sr
        df   = Sr/self.Nfft
        jt   = np.arange(Nt)
        tw   = dt*self.Nfft
        tt   = jt*dt

        Nspec = np.int(Nt/(self.Nfft*Nav))
        sk    = np.int(self.Tskip*df/2)
        Nspek = np.int(Nspec - sk)
        jspek = np.linspace(sk,Nspec-1,Nspek)
        Bav   = self.Nfft*Nav
        tspek = (jspek+0.5)*Bav*dt
        Npadd = np.int(self.Nfft*(Padd-1))
        dpadd = np.zeros(Npadd)

        print('Skip spec: ' + str(sk))
        print('Last spec: ' + str(Nspec))

        if Padd == 1:
            Npadd = 1

        jps  = np.arange(self.Nfft)
        Win  = np.power(np.cos(np.pi/self.Nfft*(jps-0.5*self.Nfft+0.5)),2)
        Nfp  = self.Nfft*Padd/2+1
        jfs  = np.arange(Nfp)
        dfs  = 1./(tw*Padd)
        ffs  = jfs*dfs

        # MakeSpec is the core function that reads the tone and outputs the spectra
        Sp  = af.MakeWideSpec(self.dataDir + self.inputfile,Nspec,self.Nfft,Nav,Ovlp,Win,Padd,dpadd)
        xSp = np.mean(Sp)
        Sp  = np.divide(Sp,xSp)
        Spa = np.divide(Sp.sum(axis=0),Nspec)

        if self.fverbose == 1:
            plt.plot(ffs,np.log10(Spa))
            plt.ylabel('Spectrum')
            plt.xlabel('Frequency [Hz]')
            plt.title('Averaged spectra')
            plt.show()

        HalfWindow    = 40
        LineAvoidance = 10

        xf   = np.zeros((Nspek,3))
        rmsd = np.zeros((Nspek,3))

        dxc    = np.zeros(Nspek)
        SNR    = np.zeros(Nspek)
        Smax   = np.zeros(Nspek)
        Fdet   = np.zeros(Nspek)
        MJD    = np.zeros(Nspek)
        Weight = np.ones((Nspek,1))

        for ip in np.arange(sk,Nspec):
            jp       = ip - sk
            xf[jp]   = af.FindMax(Sp[ip],ffs)
            Smax[jp] = xf[jp,2]
            Fdet[jp] = dfs*xf[jp,1] + ffs[0]
            if self.powcen == 1:
                dxc      = af.PowCenter(Sp[ip],xf[jp,1],3)*dfs
                Fdet[jp] = Fdet[jp] + np.transpose(dxc)
            rmsd[jp] = af.GetRMS(Sp[ip],xf[jp,1],HalfWindow/dfs,LineAvoidance/dfs)
            SNR[jp]  = (xf[jp,2] - rmsd[jp,0])/rmsd[jp,1]

        mSNR  = SNR.mean()
        mFdet = Fdet.mean()

        MJD[0:Nspek] = self.MJD

        if self.vexade == 1:
            Fdet   = Fdet - mFdet
        else:
            Weight = np.power(SNR,2)/mSNR

        Ffit  = af.PolyfitW1(tspek,Fdet,Weight,self.Npf2)
        rFdet = Fdet - Ffit
        Cf2   = af.PolyfitW1C(tspek,Fdet,Weight,self.Npf2)

        if self.verbose == 1:
            plt.subplot(4,1,1)
            plt.plot(ffs,np.log10(Spa))
            plt.ylabel('Spectrum')
            plt.xlabel('Frequency [Hz]')
            plt.subplot(4,1,2)
            plt.plot(tspek,SNR,'rx')
            plt.xlabel('Time [s]')
            plt.ylabel('SNR')
            plt.subplot(4,1,3)
            plt.plot(tspek,Fdet,'r.')
            plt.plot(tspek,Ffit,'b')
            plt.xlabel('Time [s]')
            plt.ylabel('Freq. [Hz]')
            plt.subplot(4,1,4)
            plt.plot(tspek,rFdet,'k.')
            plt.xlabel('Time [s]')
            plt.ylabel('Residual Freq [Hz]')
            plt.show()

        if self.fverbose == 1:
            plt.subplot(3,1,1)
            plt.plot(tspek,SNR,'rx')
            plt.xlabel('Time [s]')
            plt.ylabel('SNR')
            plt.subplot(3,1,2)
            plt.plot(tspek,Fdet,'r.')
            plt.plot(tspek,Ffit,'b')
            plt.xlabel('Time [s]')
            plt.ylabel('Freq. [Hz]')
            plt.subplot(3,1,3)
            plt.plot(tspek,rFdet,'k.')
            plt.xlabel('Time [s]')
            plt.ylabel('Residual Freq [Hz]')
            plt.show()

        if self.vexade == 1:
            Cf2[0] = Cf2[0] + mFdet

        print('\033[94mStd dev  : ' + str(np.std(rFdet)) + '\033[0m')
        print('\033[94mSNR mean : ' + str(np.mean(SNR)) + '\033[0m')

        Cfs2   = np.zeros(self.Npf2+1)
        Cpp2   = np.zeros(self.Npp2+1)
        Ffirst = np.zeros(Nspek)

        for ip in np.arange(self.Npf2+1):
            Cfs2[ip]   = Cf2[ip]*np.power(self.Tspan-self.Tskip,-ip)
            Cpp2[ip+1] = np.multiply(Cfs2[ip],np.power(ip+1,-1.))

        if self.Npp1 < self.Npp2:
            Cfs2[self.Npp1:self.Npp2] = 0

        for ip in np.arange(Nspek):
            for jp in np.arange(1,self.Npp1):
                Ffirst[ip] = Ffirst[ip] + self.Cfs1[jp]*np.power(tspek[ip],jp)

        # Fvideo includes the 1st polynomials the Fdet only the 2nd approach
        Fvideo = self.StartF + Ffirst + Fdet
        if self.vexade == 1:
            Fvideo = Fvideo + mFdet

        tts = tspek + self.StartT + self.Tadd

        # Storing the day of the day, timestamp, SNR, Spectral max, Fdet, Residual Fdets
        self.fdets2 = np.array([MJD,tts,SNR,Smax,Fvideo,rFdet])

        self.writing_fdets(2)

        cppname = self.dataDir + self.inputfile[0:self.fil_lng] + '.poly' + str(self.Npp2) + '.rev2.txt'
        cfsname = self.dataDir + self.inputfile[0:self.fil_lng] + '.X' + str(self.Npf2) + 'cfs.rev2.txt'
        np.savetxt(cppname,Cpp2)
        np.savetxt(cfsname,Cfs2)

        FO      = np.floor(self.BW/self.BWi)
        Nffto   = np.floor(self.Nfft/FO)
        Nsegm   = np.floor(Nt/self.Nfft)*Ovlp-(Ovlp-1)
        jpso    = np.arange(Nffto)
        Npfo    = Nffto*0.5 + 1
        BWih    = 0.5*self.BWi
        Nto     = Nt/FO
        Ntk     = (self.Tspan-self.Tskip)*Sr/FO
        dto     = dt*FO
        jto     = np.arange(Ntk)
        tto     = jto*dto
        dfto    = 1/(self.Tspan-self.Tskip)
        fto     = dfto*jto
        Oshifti = self.Nfft/Ovlp

        Wini = np.cos(np.pi/self.Nfft*(jps-0.5*self.Nfft+0.5))
        Wino = np.cos(np.pi/Nffto*(jpso-0.5*Nffto+0.5))

        Cf1 = np.zeros(self.Npf2+1)
        Cf2 = np.zeros(self.Npf2+1)

        for ip in np.arange(self.Npp1):
            Cf1[ip] = self.Cpp1[ip+1]*(ip+1)/(2*np.pi)

        for ip in np.arange(self.Npp2):
            Cf2[ip] = Cpp2[ip+1]*(ip+1)/(2*np.pi)

        Cf    = Cf1 + Cf2
        Cf[0] = Cf2[0]
        Cpp   = np.zeros(self.Npp2+1)

        for ip in np.arange(1,self.Npp2+1):
            Cpp[ip] = 2*np.pi*Cf[ip-1]/ip

        Fcc = Cpp2[1]

        ts = np.divide(tt, self.Tspan - self.Tskip)
        res = []
        for jp in np.arange(2,self.Npf2+1):
            res.append(Cpp[jp] * np.power(ts, jp))
        PhDopp = np.multiply(np.sum(np.array(res), axis=0), self.Tspan - self.Tskip)

        Bsc     = np.floor((Fcc-BWih)/df-1) # if we add one here we get exactly one bin less than in Matlab
        Bec     = np.floor(Bsc+Npfo)        # the end bin is exactly the same as python dont count the last
        Fstartc = (Bsc+1)*df                # If we add one here we get exactly the same as matlab
        Pssc    = Fstartc*Oshifti*dt - np.floor(Fstartc*Oshifti*dt)
        Esc     = np.exp(1j*2*np.pi*Pssc)
        Esc     = -Esc

        spf = af.MakeWideFiltX(self.dataDir + self.inputfile,PhDopp,Bsc,Bec,Esc,Nto,Bav,Nspec,Wini,Wino,Nsegm,self.Nfft,Nffto,Ovlp)

        spf   = spf[sk*Nffto*Nav:]
        ssf   = np.fft.fft(spf)
        ssfp  = np.power(np.abs(ssf),2)
        xssfp = ssfp.max()
        ssfp  = np.divide(ssfp,xssfp)

        if self.fverbose == 1:
            plt.plot(fto,np.log10(ssfp))
            plt.xlim([0,self.BWi])
            plt.ylim([-10,5])
            plt.title('Spacecraft tone after phase correction')
            plt.show()

        # Determine the frequency of the max power after phase correction
        xft     = af.FindMax(ssfp,fto)
        fmax    = dfto*xft[1]
        spnoise = ssfp[200:900]
        SNR     = np.divide(1,np.std(spnoise))
        dBSNR   = 10*np.log10(SNR)

        #print 'Frequency  MAX after PHC: ' + str(fmax)

        #==============================================================================
        # Select frequencies to rotate the tone
        # targetFreq 0.5 * outputBandwidth
        # rotationFreq = Initial tone Freq - targetFreq
        #==============================================================================
        Ftarg = np.floor(0.05*self.BWo)
        Frot  = fmax - Ftarg

        #==============================================================================
        # sfc:  down converted signal
        # ssf:  down converted FFT
        # ssfp: down converted Power spectrum (normalized)
        #==============================================================================
        sfc   = spf*np.exp(-1j*2*np.pi*Frot*tto)
        ssf   = np.fft.fft(sfc)
        ssfp  = np.power(np.abs(ssf),2)
        ssfp  = ssfp/ssfp.max()

        if self.fverbose == 1:
            plt.semilogy(fto,ssfp)
            plt.xlim([0,self.BWo/4])
            plt.ylim([-10,0])
            plt.title('Power spectrum down converted S/C tone')
            plt.ylabel('Power (log scale)')
            plt.xlabel('Frequency [Hz]')
            plt.show()

        #==============================================================================
        # Filtering frequencies above output bandwidth
        # ssf:  down converted filtered FFT
        # ssff: down converted filtered FFT
        # bwo:  output Frequency bin
        #==============================================================================
        ssff       = ssf
        bwo        = int(np.where(fto == self.BWo)[0])
        ssff[bwo:]  = 0

        #==============================================================================
        # Return to time domain
        # sfc: down converted and filtered signal in time-domain
        #==============================================================================
        sfc = np.fft.ifft(ssff)

        Ampl = np.abs(sfc)
        Ph   = np.angle(sfc)
        Phr  = np.unwrap(Ph)

        if self.fverbose == 1:
            plt.plot(tto,Ampl,'r')
            plt.title('Signal Amplitude')
            plt.ylabel('Amplitude')
            plt.xlabel('Time')
            plt.show()

        #==============================================================================
        # Phase reduction
        # Ph:   phase signal
        # Phr:  unwrapped phase / residual phase
        # dPhr: linear trend free phase (it could be + or -)
        #==============================================================================
        Ph   = np.angle(sfc)
        Phr  = np.unwrap(Ph)
        dPhr = Phr - 2*np.pi*Ftarg*tto

        #==============================================================================
        # Generate the phase polynomial fit (3rd correction)
        # calculate the weighted averaged polynomial fit
        # rdPhr: residual of the linear trend free phase
        #==============================================================================
        wto   = np.ones(Ntk)
        PhFit = af.PolyfitW(tto,dPhr,wto,self.Npp3)
        Cf3   = af.PolyfitWC(tto,dPhr,wto,self.Npp3)

        rdPhr = dPhr - PhFit

        #==============================================================================
        #
        # Dopp3: residual of the linear trend free phase
        #==============================================================================
        downRatio     = 10
        numSamples    = len(rdPhr)
        numAvgSamples = np.int(numSamples/downRatio)
        Dopp3         = np.zeros(numAvgSamples)
        ttr           = 0
        intTime       = 10

        for ip in range(numAvgSamples):
            ttr[ip] = tto[ip*downRatio]
            x0      = np.int(np.floor(intTime/dto*ip+0.5))
            x1      = np.int(np.floor(intTime/dto*ip+1.5))
            Dopp3[ip+1] = (rdPhr[x0] -  rdPhr[x1])/(2*np.pi*downRatio)

        print('\033[94mDoppler rev. 3 rms: ' + str(np.std(Dopp3)) + '\033[0m')

        #==============================================================================
        # Set a boundaries to discard samples at the edges
        # Initial time + 15, End time - 15
        # Store the phase data in Phases.SPA.YYYY.MM.DD.ST.txt format
        #==============================================================================
        tmp   = np.where(tto == 15)
        bmin  = tmp[0]
        tmp   = np.where(tto == self.Tspan - self.Tskip - 15)
        bmax  = tmp[0]


        print('\033[94mPhase rms: ' + str(np.std(rdPhr[bmin:bmax])) + '\033[0m')
        self.phase = np.array([tto[bmin:bmax],rdPhr[bmin:bmax]])
        p.writing_phase()

        if self.verbose == 1:
            plt.title('Summary of the PLL tone detection')
            plt.subplot(2,1,1)
            plt.plot(tto,dPhr,'r')
            plt.plot(tto,PhFit,'b')
            plt.ylabel('phase [rad]')
            plt.subplot(2,1,2)
            plt.plot(tto,rdPhr,'c')
            plt.ylabel('phase [rad]')
            plt.xlabel('time [sec]')
            plt.show()

        #==============================================================================
        # Correct the downconverted signal with the linear trend free phase
        # sfcc: linear phase signal
        # ssf:  linear phase signal FFT
        #==============================================================================
        sfcc = sfc*np.exp(-1j*PhFit)
        ssf  = np.fft.fft(sfcc)
        ssfp = np.power(np.abs(ssf),2)
        ssfp = ssfp/ssfp.max()

        rmsf = af.GetRMSf(ssfp,fto,Ftarg,0.4*self.BWo,0.1*self.BWo)
        SNR  = np.divide(1-rmsf[0],rmsf[1])

        #==============================================================================
        # Very quick and dirty estimate of the phase scintillation
        # It gives an overlook of the quality of the data
        #==============================================================================
        bss  = np.floor(0.003/df + 1.5)
        bse  = np.floor(3.003/df + 1.5)
        lph  = np.size(self.phase[1,:])
        wcos = sps.cosine(lph)
        sp   = np.fft.fft(self.phase[1,:])
        spw  = np.fft.fft(np.multiply(self.phase[1,:],wcos))
        pspw = np.power(np.abs(spw),2)
        pspw = 2*pspw/20.
        dta = (self.phase[0,1]-self.phase[0,0])
        Tsa  = dta*lph
        dfa  = 1/Tsa
        ff   = np.arange(0,self.BWi*2-dfa,dfa)

        FiltScint = np.zeros(lph)
        FiltScint[bss:bse] = 2

        spsc   = np.zeros((np.size(self.phase[1,:])),dtype=np.complex64)
        spsc   = np.multiply(sp,FiltScint)
        phs    = np.real(np.fft.ifft(spsc,axis=0))
        rmsphs = np.std(phs, axis=0)

        if self.fverbose == 1:
            print('Phase scintillation RMS: {}'.format(rmsphs))
            plt.loglog(ff,pspw)
            plt.vlines(0.003,1,1e12,colors='red')
            plt.vlines(3.003,1,1e12,colors='red')
            plt.xlim([0,10])
            plt.show()
            plt.vlines

        #print 'Total running time: ' + str(time.clock() - start)

        #==============================================================================
        # Ultra phase stop - Filtering to a very narrow bandwidth around the tone
        #==============================================================================
        BWn = 0.5
        nsfc  = np.fft.fft(sfcc)

        bwna = np.where(fto == ((np.int(Ftarg) - BWn/2)))
        bwnl = np.int(bwna[0])
        bwna = np.where(fto == ((np.int(Ftarg) + BWn/2)))
        bwnh = np.int(bwna[0])

        # High pass filter above 0.5 Hz
        nsfc[0:bwnl] = 0
        nsfc[bwnh:]  = 0

        nfc    = np.fft.ifft(nsfc)
        Phn    = np.angle(nfc)
        Phrn   = af.DeWrap(Phn)
        dPhrn  = Phrn - np.pi*(Ftarg*2)*tto
        rdPhrn = dPhrn

        if self.fverbose == 1:
            plt.title('Phase detection after filtering the signal')
            plt.plot(tto,rdPhrn,'c')
            plt.plot(tto,rdPhr,'r')
            plt.ylabel('phase [rad]')
            plt.xlabel('time [sec]')
            plt.show()

p = function_PLL(dataDir,filename)
p.set_variables()
p.data_processing()
