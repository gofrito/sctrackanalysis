#!/usr/bin/env python3
"""
Created on Sat Jun 14 07:49:56 2014

Estimate the First Polynomials
@author: molera
"""
# Import required tools
import numpy as np
import AnalysisFunction as af
import matplotlib.pyplot as plt
import os

def SelectGraph(prompt):
   graph = input(prompt)
   return graph

def select_spacecraft(dataFile):
    fbase = 'xxxx.xx'
    nbase = 'spa'
    if dataFile[0] == 'v':
        fbase = '8415.99'
        nbase  = 'vex'
    if dataFile[0] == 'r':
        fbase = '8396.59'
        nbase = 'ras'
    if dataFile[0] == 'm':
        fbase = '8415.99'
        nbase = 'mex'
    if dataFile[0] == 'g':
        fbase = '2xxx.xx'
        nbase = 'gns'
    if dataFile[0] == 'h':
        fbase = '8468.50'
        nbase = 'her'
    return fbase,nbase

class fp(object):
    '''
    First Polynomial class estimates the Phase polynomials
    from a given spectra input.
    
    input:
        dataFile - input spectrum in binary format
        dataDir - directory to look for the file
        BW - bandwidth input
    '''
    def __init__(self, dataFile, dataDir, Fsmin=0, Fsmax=0, dataOut='.', Npf=5, BW=4e6, Nspec=0, iterative='No'):
        # Read the input variables from the dataFile
        # These values are generated after swspec
        FFTpt      = int(dataFile[23:30])
        Station    = dataFile[8:10]
        DataFormat = dataFile[11:15]
        ScanNo     = dataFile[18:22]
        dts        = int(dataFile[33])
        channel    = dataFile[38]
        
        if Nspec == 0:      
            fsize = os.path.getsize(os.path.join(dataDir, dataFile))
            Ts    = 0.5*fsize/(FFTpt + 1)
            Nspec = np.floor(Ts/dts)-1
        else:
            Ts = int((Nspec+1)*dts)

        # Nspec has to be calculated - now i just assigned arbitrary to 1120        
        SR    = 2*BW
        dtsam = 1/SR
        Hwin  = 1e3
        Avoid = 100
        df    = SR/FFTpt
        Nfft  = FFTpt/2+1;
        jf    = np.arange(0,Nfft)
        ff    = df*jf
        tsp   = dts*(0.5+np.arange(0,Nspec))
        Npp   = Npf + 1
        
        # Read file containing the spectra in binary format
        fd = open(os.path.join(dataDir, dataFile),'rb')
        if fd < 0:
            print('- Spectra file not found: {}'.format(dataFile))
        
        if Fsmin == 0:
            read_data = np.fromfile(file=fd, dtype='float32', count=Nfft)
            read_data = read_data[100:-100]
            print(read_data.max())
            print(read_data.argmax())
            bsc = read_data.argmax()
            Fsmin = (bsc - 10000)*df
            Fsmax = (bsc + 10000)*df
            
        print(Fsmin, Fsmax)
        bfs = int(Fsmin/df)
        bfm = int(Fsmax/df)
        Sps = np.zeros((Nspec,bfm-bfs))
        Aspec = np.zeros((bfm-bfs))
        ffs = ff[bfs:bfm]
        Tspan = max(tsp)
        fd.seek(0)
        
        for ip in np.arange(Nspec):
            read_data = np.fromfile(file=fd, dtype='float32', count=Nfft)
            vspec     = read_data[bfs:bfm]
            Sps[ip]   = vspec*np.hanning(bfm-bfs)
            Aspec     = Sps[ip] + Aspec
            
        fd.close()
        
        Aspec = Aspec/Nspec
        mSp = Sps.max()
        Sps = Sps/mSp
        lSpec = read_data
        
        if iterative == 'Yes':
             print('Last spectrum read from the file - full bandwdith')
             plt.semilogy(ff,lSpec)
             plt.ylabel('Spectrum')
             plt.xlabel('Frequency [Hz]')
             plt.title('Last integration averaged spectra')
             plt.show()
             
        # Spectra file opened and copied to an array
        xfc  = np.zeros((Nspec,3))
        RMS  = np.zeros((Nspec,3))
        Smax = np.zeros((Nspec))
        SNR  = np.zeros((Nspec))
        Fdet = np.zeros((Nspec))
        dxc  = np.zeros((Nspec))
        MJD  = np.zeros((Nspec))
        
        if iterative == 'Yes':
            print('Averaged time-integrated spectra - Span zoomed')
            plt.semilogy(ffs,Aspec)
            plt.ylabel('Spectra')
            plt.xlabel('Frequency [Hz]')
            plt.title('Zoomed-averaged time-integrated spectra')
            plt.show()

        # Seeking for the Max value peak on
        # the spectra and estimate the RMS
        
        for ip in np.arange(Nspec):
            xfc[ip] = af.FindMax(Sps[ip],ffs)
            Smax[ip] = xfc[ip,2]
            Fdet[ip] = df*xfc[ip,1] + ffs[0]
            RMS[ip]  = af.GetRMS(Sps[ip],ffs,Fdet[ip],Hwin,Avoid)
            SNR[ip]  = (xfc[ip,2] - RMS[ip,0])/RMS[ip,1]
            
        mSNR = SNR.mean()
        
        # Calculating the centre of the gravity correction
        for ip in np.arange(Nspec):
            dxc[ip] = af.PowCenter(Sps[ip],xfc[ip,1],3)

        dxc = dxc*df
        # Adding the correction to our Frequency detections
        FdetC  = Fdet + dxc
        Weight = np.power(SNR/mSNR,2)
              
        print('- Calculating the Polynomial fit')
        Cfs = np.zeros(Npf+1)
        Cps = np.zeros(Npp+1)
        Cpr = np.zeros(Npp+1)
         
        Cf   = af.PolyfitW1C(tsp,FdetC,Weight,Npf)
        Ffit = af.PolyfitW1(tsp,FdetC,Weight,Npf)
        rFit = FdetC - Ffit

        for jpf in range(Npf+1):
            Cfs[jpf] = Cf[jpf]*np.power(Tspan,-jpf)
           
        for jpf in range(1,Npp+1):
            Cps[jpf] = 2*np.pi*Cfs[jpf-1]/jpf
            Cpr[jpf] = Cps[jpf]*np.power(dtsam,jpf)    

        if iterative == 'Yes':
             ax1 = plt.subplot2grid((3,1),(0,0))
             ax1.plot(tsp,FdetC,'bx')
             ax2 = plt.subplot2grid((3,1),(1,0))
             ax2.plot(tsp,rFit,'ro')
             ax3 = plt.subplot2grid((3,1),(2,0))
             ax3.plot(tsp,SNR,'r.')
             plt.tight_layout()
             plt.draw()
             plt.show()
             
        #    def save_coeffients(self):
        basefilen = np.size(dataFile) - 12
        cppname = '{}{}.poly{}.txt'.format(dataDir,dataFile[0:basefilen],str(Npf))
        cfsname = '{}{}.X{}cfs.txt'.format(dataDir,dataFile[0:basefilen],str(Npf-1))
        np.savetxt(cppname,Cpr)
        np.savetxt(cfsname,Cfs)
            
        print('- Reading starting time of the scan')
        fd = open('{}{}_starttiming.txt'.format(dataDir,dataFile[0:basefilen]),'r')
        if fd < 0:
           print('Error the start time file')
            
        fd.readline()
        Day, Tim, Sec = [float(x) for x in fd.readline().split()]
        fd.close()
        Start = Tim
        MJD[0:Nspec] = Day

        self.fbase,self.nbase = select_spacecraft(dataFile)
        print('- Create and store the Frequency Detections after the 1st iteration')
        fdetsname = '{}Fdets.{}20{}.{}.{}.{}.r0i.txt'.format(dataDir,self.nbase,dataFile[1:3],dataFile[3:5],dataFile[5:7],dataFile[8:10],dataFile[18:22])
        tsp = tsp + Start
        Fds = np.array([MJD,tsp,SNR,Smax,FdetC,rFit])
        Fdets = Fds.conj().T
        np.savetxt(fdetsname, Fdets, newline='\n')
        fd = open(fdetsname,'r+')
        fd.seek(0)
        a = fd.read()
        fd.seek(0)
        header = '* Observation conducted on {} at {} rev. 0\n\
                  * S/C base frequency: {} MHz\n\
                  * Format : Time(UTC) [s]  | Signal-to-Noise ratio  |       Spectral max     |  Freq. detection [Hz]  |  Doppler noise [Hz] \n\
                  * \n'.format(dataFile[9:19],dataFile[8:10], self.base)
        fd.write(header)
        fd.write(a)
        fd.close()
