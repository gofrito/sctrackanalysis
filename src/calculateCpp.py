#!/usr/bin/env python3
"""
    Created on Fri Jul  4 11:44:23 2014

    CalculateCpp.py - Calculate the first polynomial from the given spectra in
                      binary format.
                      User calculateCpp.py -h to see the help menu
    @author: molera
    """

import numpy as np
import AnalysisFunction as af
import matplotlib.pyplot as plt
import os, argparse, re

class calculateCpp:
    def __init__(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("-p",  "--plot", help="Plot some results for S/C", action="store_true", default=False)
        parser.add_argument("-vx", "--vexade", help="Fast S/C don't use weighted functions SNR based", action="store_true", default=False)
        parser.add_argument("-pc", "--powercentre", help="Don't use near bins to estimate Max Freq", action="store_true", default=False)
        parser.add_argument("-bw", "--bandwidth", help="Observing bandwidth", type=float, default=32)
        parser.add_argument("-t0", "--initialtime", help="Initial time", type=int, default=0)
        parser.add_argument("-t1", "--endtime", help="End time", type=int, default=0)
        parser.add_argument("-f0", "--minfrequency", help="Minimum Frequency", type=float, default=4)
        parser.add_argument("-f1", "--maxfrequency", help="Maximum Frequency", type=float, default=6)
        parser.add_argument("-rf", "--radiofrequency", help="Starting Radio Frequency", type=float, default=0)
        parser.add_argument("-p1", "--polynomial", help="First order polynomial", type=int, default=6)
        parser.add_argument("-ft", "--forcetime", help="force integration time and do not use the filename", type=float, default=0)
        parser.add_argument("-s",  "--spacecraft", help="Spacecraft")
        parser.add_argument("-m",  "--mask", help='outline anomolous points on the data and do masking', action="store_true", default=False)
        parser.add_argument("filename", nargs='?', help="Input spectra file", default="check_string_for_empty")
        args = parser.parse_args()

        self.plot        = args.plot
        self.vexade      = args.vexade
        self.PowCen      = args.powercentre
        self.mask        = args.mask

        self.BW  = args.bandwidth*1e6
        self.t0  = args.initialtime
        self.t1  = args.endtime
        self.Npp = args.polynomial
        self.Npf = self.Npp - 1
        self.ft  = args.forcetime

        path,self.filename = os.path.split(args.filename)
        self.path = '{}/'.format(os.path.abspath(path))

        match = re.search(r'^([^_]*)_([^_]*)_(?:([^_]*)_)?(?:No|Na|Nb|Nc|Nd|Ne|Nf)?(\d+)_(\d+)(?:pt)?_(\d+)s?_(?:ch)?([^_]+)_(?:swspec|scspec)', self.filename, re.I)

        self.session    = match.group(1)
        self.station    = match.group(2)
        self.rawFormat  = match.group(3)
        self.scanNumber = match.group(4)
        self.FFTpt      = np.int(match.group(5))
        self.dts        = np.float(match.group(6))
        self.channel    = match.group(7)

        if self.ft > 0 :
            self.dts = self.ft

        if args.spacecraft:
            self.spacecraft = args.spacecraft
            self.Fstart = 0.0
        else:
            if self.session[0] == 'v':
                self.Fsmin  = 2.05e6
                self.Fsmax  = 2.25e6
                self.Fstart = 8415.99
                self.spacecraft = 'vex'
            elif self.session[0] == 'r':
                self.Fsmin  = 1.75e6
                self.Fsmax  = 1.85e6
                self.Fstart = 8419.99
                self.spacecraft = 'ros'
            elif self.session[0] == 'm':
                self.Fsmin  = 2.45e6
                self.Fsmax  = 2.60e6
                self.Fstart = 8415.99
                self.spacecraft = 'mex'
            elif self.session[0] == 'g':
                self.Fstart = 8460.99
                self.spacecraft = 'gai'
            elif self.session[0] == 'h':
                self.Fstart = 8468.50
                self.spacecraft = 'her'
            elif self.session[0] == 'k':
                self.Fstart = 8396.99
                self.spacecraft = 'ras'
            else:
                self.Fstart = 0.0
                self.spacecraft = 'non'

        if args.minfrequency:
            self.Fsmin = args.minfrequency*1e6
        if args.maxfrequency:
            self.Fsmax = args.maxfrequency*1e6
        if args.radiofrequency:
            self.Fstart = args.radiofrequency

        if self.t1 == 0:
            fsize      = os.path.getsize('{}{}'.format(self.path,self.filename))
            self.Nspec = np.int(np.floor(0.5*fsize/(self.FFTpt + 1)))
            self.t1    = self.Nspec*self.dts
        else:
            self.Nspec = np.int(self.t1/self.dts)

    def writing_fdets(self,rev):
        if len(self.session) == 7:
            day = '20{}.{}.{}'.format(self.session[1:3],self.session[3:5],self.session[5:7])
        else:
            day = ''

        freq_header = 'Base frequency: {:.2f} MHz \n'.format(self.Fstart)

        fdets_fn = '{}Fdets.{}{}.{}.{}.r0i.txt'.format(self.path,self.spacecraft,day,self.station,self.scanNumber)
        fdets_header = 'Observation conducted on {} at {} rev. {} \n{}\
            Format : Modified JD  |       Time(UTC) [s]    | Signal-to-Noise ratio  |       Spectral max     |  Freq. detection [Hz]  |    Doppler noise [Hz]   |\n'\
            .format(str(day),self.station,str(rev),freq_header)

        np.savetxt(fdets_fn, np.transpose(self.fdets), newline='\n', header=fdets_header)

    def processing(self):
        SR      = 2*self.BW
        dtsam   = 1/SR
        df      = SR/self.FFTpt
        Hwin    = np.int(1e3/df)
        Avoid   = np.int(100/df)
        Nfft    = np.int(self.FFTpt/2+1)
        jf      = np.arange(Nfft)
        self.ff = df*jf
        b0      = np.int(np.round(self.t0/self.dts))
        b1      = self.Nspec
        tsp     = self.dts*(0.5+np.arange(b0,b1))
        Nspek   = b1 - b0

        # Read file
        fd = open('{}{}'.format(self.path,self.filename),'rb')

        bfs      = np.int(self.Fsmin/df)
        bfm      = np.int(self.Fsmax/df)
        self.bfs = bfs
        self.bfm = bfm
        Sps      = np.zeros((self.Nspec,bfm-bfs))

        self.Aspec  = np.zeros(bfm-bfs)
        self.ffs    = self.ff[bfs:bfm]

        for ip in np.arange(self.Nspec):
            read_data = np.fromfile(file=fd, dtype='float32', count=Nfft)
            vspec     = read_data*np.hanning(Nfft)
            Sps[ip]   = vspec[bfs:bfm]

        fd.close()

        self.Aspec = np.sum(Sps,axis=0)/self.Nspec
        mSp        = Sps.max()
        Sps        = Sps/mSp
        self.lSpec = read_data

        # Spectra file opened and copied to an array
        xfc  = np.zeros((Nspek,3))
        RMS  = np.zeros((Nspek,3))
        Smax = np.zeros(Nspek)
        SNR  = np.zeros(Nspek)
        Fdet = np.zeros(Nspek)
        dxc  = np.zeros(Nspek)
        MJD  = np.zeros(Nspek)
        avbin = 0

        # Seeking for the Max and estimating the RMS
        for ip in np.arange(b0,b1):
            jp       = ip - b0
            xfc[jp]  = af.FindMax(Sps[ip],self.ffs)
            if (self.mask == 1 & jp > 0):
               if jp == 1 :
                  avbin = xfc[jp,1] - xfc[jp-1,1]
               else:
                  'Check Doppler shift with previous bin'
                  tmp = xfc[jp,1] - xfc[jp-1,1]
                  if (np.abs(tmp) > avbin*1.5 or np.abs(tmp) < avbin*0.5):
                     xfc[jp,1] = xfc[jp-1,1] + avbin
                     xfc[jp,2] = np.average(Smax)
                     print('We got in here')
                  else:
                     avbin = (tmp + avbin)/2
            else:
               'Life continues'
            Smax[jp] = xfc[jp,2]
            Fdet[jp] = df*xfc[jp,1] + self.ffs[0]
            RMS[jp]  = af.GetRMS(Sps[ip],xfc[jp,1],Hwin,Avoid)
            SNR[jp]  = (xfc[jp,2] - RMS[jp,0])/RMS[jp,1]
            dxc[jp]  = af.PowCenter(Sps[ip],xfc[jp,1],3)

        ibin = xfc[0,1]

        # Adding the correction to our Frequency detections
        mSNR     = SNR.mean()
        dxc      = dxc*df
        FdetC    = Fdet + dxc
        self.SNR = SNR

        if self.vexade == 1:
            mFdet  = np.mean(FdetC)
            FdetC  = FdetC - mFdet
            Weight = np.ones(Nspek)
        else:
            Weight = np.power(SNR,2)/mSNR

        self.Fdet = FdetC

        Cfs = np.zeros(self.Npp)
        Cps = np.zeros(self.Npp+1)
        Cpr = np.zeros(self.Npp+1)

        tsp   = tsp - self.t0
        Tspan = np.max(tsp)
        # It will be interesting to compare performance with std numpy.polyfit function
        Cf    = af.PolyfitW1C(tsp,FdetC,Weight,self.Npf)
        Ffit  = af.PolyfitW1(tsp,FdetC,Weight,self.Npf)

        self.rFit  = FdetC - Ffit

        if self.vexade == 1:
            Cf[0] = Cf[0] + mFdet
            Ffit  = Ffit  + mFdet
            FdetC = FdetC + mFdet

        for jpf in np.arange(self.Npp):
            Cfs[jpf] = Cf[jpf]*np.power(Tspan,-jpf)

        for jpf in np.arange(1,self.Npp+1):
            Cps[jpf] = 2*np.pi*Cfs[jpf-1]/jpf
            Cpr[jpf] = Cps[jpf]*np.power(dtsam,jpf)

        # Save the polynomial coefficients into a file
        basefilen = np.size(self.filename) - 12 # 12bits: _swspec.bin

        cppname   = '{}{}.poly{}.txt'.format(self.path,self.filename[0:basefilen],str(self.Npp))
        cfsname   = '{}{}.X{}cfs.txt'.format(self.path,self.filename[0:basefilen],str(self.Npf))

        np.savetxt(cppname,Cpr)
        np.savetxt(cfsname,Cfs)

        # Reading starting time of the scan
        timebin = '{}{}_starttiming.txt'.format(self.path,self.filename[0:basefilen])
        Tsinfo  = np.loadtxt(timebin,skiprows=1)
        MJD[:]  = Tsinfo[0]
        Start   = Tsinfo[1]

        # Save the frequency detections revision 0 into a file
        ttsp  = tsp + Start + self.t0
        self.tsp = tsp
        self.fdets = np.array([MJD,ttsp,SNR,Smax,FdetC,self.rFit])

        self.writing_fdets(0)

        print('\n\033[94m- S/C sky frequency   : {:.2f} Mhz'.format(self.Fstart+FdetC[0]/1e6))
        print('- S/C band frequency  : {:.3f} MHz'.format(FdetC[0]/1e6))
        print('- S/C initial bin     : {} '.format(int(ibin)))
        print('- Number of spectra   : {} '.format(int(self.Nspec)))
        print('- Average SNR         : {:.1f} '.format(mSNR))
        print('- Doppler noise       : {:.3f}  Hz'.format(self.rFit.std()))
        print('- Doppler shift       : {:.2f}  Hz\033[0m\n'.format(np.abs(FdetC[0]-FdetC[-1])))

    def print_plots(self):
        if self.plot == 1:
            print('Select the figure that you would like to display')
            print('1- Last spectrum read from the file - full bandwdith')
            print('2- Averaged time-integrated spectra - Span zoomed')
            print('3- Frequency detections of the S/C signal')
            print('4- Summary of the detection')
            graph = input('Make a choice: 1-2-3-4\n')

            plt.figure()
            self.ff = (self.Fstart+self.ff)/1e6

            if graph == '1':
                plt.plot(self.ff,np.log10(self.lSpec))
                plt.ylabel('Spectrum')
                plt.xlabel('Freq [MHz]')
                plt.title('Last spectrum read from the file {} at {}'.format(self.session,self.station))
                plt.show()
            if graph == '2':
                plt.plot(self.ff[self.bfs:self.bfm],np.log10(self.Aspec))
                plt.ylabel('Spectra')
                plt.xlabel('Freq [MHz]')
                plt.title('Averaged time-integrated spectra {} at {}'.format(self.session,self.station))
                plt.show()
            if graph == '3':
                ax1 = plt.subplot2grid((3,1),(0,0))
                ax1.plot(self.tsp+self.t0,self.SNR,'bx')
                ax1.set_ylabel('SNR')
                ax2 = plt.subplot2grid((3,1),(1,0))
                ax2.plot(self.tsp+self.t0,self.Fstart+self.Fdet,'ro')
                ax2.set_ylabel('Fdets')
                ax3 = plt.subplot2grid((3,1),(2,0))
                ax3.plot(self.tsp+self.t0,self.rFit,'rx')
                ax3.set_ylabel('Dnoise')
                plt.tight_layout()
                plt.title('Summary II of the Frequency detections {} at {}'.format(self.session,self.station))
                plt.show()
            if graph == '4':
                ax1 = plt.subplot2grid((2,2),(0,0), colspan=2)
                ax1.plot(self.ff,np.log10(self.lSpec))
                ax1.set_ylabel('Spectrum')
                ax2 = plt.subplot2grid((2,2),(1,0))
                ax2.plot((self.Fstart+self.ffs[100:-100])/1e6,np.log10(self.Aspec[100:-100]))
                ax2.set_ylabel('Spectra')
                ax3 = plt.subplot2grid((2,2),(1,1))
                ax3.plot(self.tsp+self.t0,self.Fstart+self.Fdet,'ro')
                ax3.set_ylabel('Fdets')
                plt.tight_layout()
                plt.title('Summary I of the Frequency detections {} at {}'.format(self.session,self.station))
                plt.show()
            return graph

cCpp = calculateCpp()
cCpp.processing()
cCpp.print_plots()
