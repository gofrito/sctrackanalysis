#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 23 10:58:23 2016

Convert the files with residual phase to residual frequency
a.k.a Fdets3.
v0.1 - Unfinished version 
@author: gofrito
"""

import numpy as np
import argparse

def read_phases(phase_file):
    phase = np.loadtxt(phase_file,skiprows=4) 
    return phase

parser = argparse.ArgumentParser()
parser.add_argument('filename', help='File of residual phases', nargs='?', default='check_string_for_empty')
parser.add_argument('intTime', help='Integration time', nargs='?', type=int, default=10)
parser.add_argument('order', help='Determine accuracy order', nargs='?', type=int, default=1)
args = parser.parse_args()

filename = args.filename
avTime   = args.intTime
order    = args.order

phases   = read_phases(filename)

nscans   = np.size(phases,1)-1

print('')
print('Opening File:      {}'.format(filename))
print('Number of scans:   {}'.format(str(nscans)))
print('Integration time:  {} s'.format(str(avTime)))
print('Order of accuracy: {}'.format(str(order)))
print('')

# Check time resolution and span
tr = phases[1,0] - phases[0,0]
ts = np.size(phases,0)
tt = np.linspace(0,ts*tr,ts)
tx = np.linspace(0,ts*tr,np.floor(ts*tr/avTime))

# Tsample is the total number of output samples
Tsample = len(tx)
Fdets3  = np.zeros([Tsample,nscans])

for jp in np.arange(nscans):
    for ip in np.arange(Tsample-1):
        x0 = np.int(np.floor(avTime/tr*(ip+0.5)))
        x1 = np.int(np.floor(avTime/tr*(ip+1.5)))
        x2 = np.int(np.floor(avTime/tr*(ip+2.5)))
        x3 = np.int(np.floor(avTime/tr*(ip+3.5)))
        x4 = np.int(np.floor(avTime/tr*(ip+3.5)))
        if order == 1:
            Fdets3[ip,jp] = (-1.0/1.*phases[x0,jp+1] + 1*phases[x1,jp+1])/(2.*np.pi*avTime)
        elif order == 2:
            Fdets3[ip,jp] = (-1.5/1.*phases[x0,jp+1] + 2.*phases[x1,jp+1] - 0.5*phases[x2,jp+1])/(2.*np.pi*avTime)
        elif order == 3:
            Fdets3[ip,jp] = (-11/6.*phases[x0,jp+1]  + 3.*phases[x1,jp+1] - 3/2.*phases[x2,jp+1] + 1/3.*phases[x3,jp+1])/(2.*np.pi*avTime)
        elif order == 4:
            Fdets3[ip,jp] = (-25/12.*phases[x0,jp+1] + 4.*phases[x1,jp+1] - 3.*phases[x2,jp+1] + 4/3.*phases[x3,jp+1] - 1/4.*phases[x4,jp+1])/(2.*np.pi*avTime)
        
for jp in np.arange(nscans):
    print('\033[94m{}: {}mHz\033[94m'.format(str(jp+1),str(np.std(Fdets3[:,jp]*1e3))))

# Store the data in a TXT file
