%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       MATLAB tools for the post-processing     %
%       of the spacecraft main carrier line      %
%												 %
% Comparing different polynomial coefficients    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Cpp0_1  = textread('/Users/molera/Treball/SCtracking/data/r140921/Bd/2tone_mode1/r140921_Bd_Mk5B_No0002_3200000pt_5s_ch2.poly6.txt');
Cpp0_2  = textread('/Users/molera/Treball/SCtracking/data/r140921/Bd/2tone_mode2/r140921_Bd_Mk5B_No0002_3200000pt_5s_ch2.poly6.txt');
%Cpp0_3  = textread('/Users/molera/Treball/SCtracking/data/r140921/Bd/2tone_mode3/r140921_Bd_Mk5B_No0002_3200000pt_5s_ch2.poly6.txt');
%Cpp0_4  = textread('/Users/molera/Treball/SCtracking/data/r140921/Bd/2tone_mode4/r140921_Bd_Mk5B_No0002_3200000pt_5s_ch2.poly6.txt');
Cpp0_5  = textread('/Users/molera/Treball/SCtracking/data/r140921/Bd/r140921_Bd_Mk5B_No0002_3200000pt_5s_ch2.poly6.txt');

Np   = length(Cpp0_5)-1;
Sr   = 3.2e6;

Nt   = 1140;
jt   = 0:1:Nt-1;
tt   = jt;
tts  = tt.*Sr;

phA(1:Nt) = 0;
phB(1:Nt) = 0;
phC(1:Nt) = 0;
phD(1:Nt) = 0;
phE(1:Nt) = 0;

for jjt=1:Nt;
    for jj=3:Np
        phA(jjt) = phA(jjt) + Cpp0_1(jj)*tts(jjt).^(jj-1);
        phB(jjt) = phB(jjt) + Cpp0_2(jj)*tts(jjt).^(jj-1);
       % phC(jjt) = phC(jjt) + Cpp0_3(jj)*tts(jjt).^(jj-1);
       % phD(jjt) = phD(jjt) + Cpp0_4(jj)*tts(jjt).^(jj-1);
        phE(jjt) = phE(jjt) + Cpp0_5(jj)*tts(jjt).^(jj-1);
    end
end

figure(1);
plot(tt,phA,'b');hold on;
%plot(tt,phB,'r');
%plot(tt,phC,'k');
%plot(tt,phD,'c');
plot(tt,phE,'m');
xlabel('tt');ylabel('Polynomials');
legend('Mode 1','Mode 5');