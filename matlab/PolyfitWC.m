%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%         MATLAB tools to analyse IPS            %
%        using spacecraft phase signal           %
%                                                %
% PolyfitWC.m - G. Molera                        %
% Calculates a Weight Polynomial fit             %
% Input: vectors x and y, weight and Npolys      %
% Output: Coefficients Cp                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function [dout] = PolyfitWC(x,y,w,np)

 nx = length(x);
 xx = max(x);
 xy = min(x);
 xc = 0.5*(xx+xy);
 xa = 0.5*(xx-xy);
 xn = (x-xc)./xa ;

 Vp(1:np+1)=0;
 Mp=zeros(np+1);

 for jp=1:np+1
    tmpPoly = 0;
    for jj=1:nx
        tmpPoly = tmpPoly + ((xn(jj)^(jp-1)).*y(jj).*w(jj));
    end
    Vp(jp) = tmpPoly;
    for ip=1:np+1
        tmpPoly = 0;
        for jj=1:nx
            tmpPoly = tmpPoly + (w(jj).*(xn(jj).^(jp+ip-2)));
        end
        Mp(ip,jp) = tmpPoly;
    end
 end

 Mr = Mp^(-1);
 Cp = Mr*Vp';

 dout = Cp;
end
